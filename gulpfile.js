var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'easy-responsive-tabs.css',
        'media_screen.css',
        'jquery-ui.min.css',
        'jquery-ui.theme.min.css',
        'jquery-ui.structure.min.css',
        'style.css'
    ]);

    mix.scripts([
        'jquery.min1.js',
        'jquery-ui.min.js',
        'easyResponsiveTabs.js',
        'popup.js',
        'script.js'
    ]);

    mix.version([
        'public/css/all.css',
        'public/js/all.js'
    ]);
});
