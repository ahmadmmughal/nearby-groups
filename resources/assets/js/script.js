
var ww = document.body.clientWidth;

$(document).ready(function() {
	$(".nav li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".nav").toggle();
	});
	adjustMenu();

    $('select[name=category]').change(function(){
        $.ajax({
            url: $base_url + "/category/" + $(this).val() + "/sub_categories",
            type: 'GET',
            dataType: 'JSON'
        }).done(function(response){
            $('select[name=sub_category]').empty().append('<option value="">Select Sub Category</option>');

            $.each(response, function(i, obj)
            {
                $('select[name=sub_category]').append('<option value=' + obj.id + '>' + obj.name + '</option>');
            });
        }).fail(function(){
        }).always(function(response){
        });
    });

    $('select[name=category_id]').change(function(){
        $.ajax({
            url: $base_url + "/category/" + $(this).val() + "/sub_categories",
            type: 'GET',
            dataType: 'JSON'
        }).done(function(response){
            $('select[name=sub_category_id]').empty().append('<option value="">Select Sub Category</option>');

            $.each(response, function(i, obj)
            {
                $('select[name=sub_category_id]').append('<option value=' + obj.id + '>' + obj.name + '</option>');
            });
        }).fail(function(){
        }).always(function(response){
        });
    });

    $('select[name=country]').change(function(){
        $.ajax({
            url: $base_url + "/country/" + $(this).val() + "/cities",
            type: 'GET',
            dataType: 'JSON'
        }).done(function(response){
            $('select[name=city]').empty().append('<option value="">Select City</option>');

            $.each(response, function(i, obj)
            {
                $('select[name=city]').append('<option value=' + obj.id + '>' + obj.name + '</option>');
            });
        }).fail(function(){
        }).always(function(response){
        });
    });

    $('select[name=country_id]').change(function(){
        $.ajax({
            url: $base_url + "/country/" + $(this).val() + "/cities",
            type: 'GET',
            dataType: 'JSON'
        }).done(function(response){
            $('select[name=city_id]').empty().append('<option value="">Select City</option>');

            $.each(response, function(i, obj)
            {
                $('select[name=city_id]').append('<option value=' + obj.id + '>' + obj.name + '</option>');
            });
        }).fail(function(){
        }).always(function(response){
        });
    });
});



$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});

var adjustMenu = function() {
	if (ww < 999) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$(".nav").hide();
		} else {
			$(".nav").hide();
		}
		$(".nav li").unbind('mouseenter mouseleave');
		$(".nav li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >=999) {
		$(".toggleMenu").css("display", "none");
		$(".nav").show();
		$(".nav li").removeClass("hover");
		$(".nav li a").unbind('click');
		$(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}

