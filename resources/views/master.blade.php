<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('images/1441717552.ico') }}">
        <title>Nearby Groups</title>
        <link href="{{ elixir('css/all.css') }}" media="screen" rel="stylesheet" type="text/css" />

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-67244987-1', 'auto');
            ga('send', 'pageview');
        </script>

        <script type="text/javascript">
            $base_url = "{{ url('/') }}";
        </script>
    </head>

    <body>
        @if( session()->has('error') )
            <div class="errors">
                {{ session('error') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="errors">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!--start_top_header_contanear-->
        @include('partials._nav')

        @include('partials._modals')

        @yield('jumbotron')

        @yield('content')

        @include('partials._footer')

        <script type="text/javascript" src="{{ elixir('js/all.js') }}"></script>

        @yield('javascript')

            <script>
                $(function() {
                    $( "#datepicker" ).datepicker();
                });
            </script>
    </body>
</html>
