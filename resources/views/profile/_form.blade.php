<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Name</h1></div>
    <div class="profile_update_right_field">
        {!! Form::text('name', null, ['class' => 'profile_update_right_input_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>About Me</h1></div>
    <div class="profile_update_right_field">
        {!! Form::textarea('bio', null, ['class' => 'profile_update_right_textarea_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>hobbies</h1></div>
    <div class="profile_update_right_field">
        {!! Form::text('hobbies', null, ['class' => 'profile_update_right_input_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Country</h1></div>
    <div class="profile_update_right_field">
        {!! Form::select('country_id', $countries, null, ['class' => 'profile_update_right_select_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>City</h1></div>
    <div class="profile_update_right_field">
        {!! Form::select('city_id', $cities, null, ['class' => 'profile_update_right_select_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Email-iD</h1></div>
    <div class="profile_update_right_field">
        {!! Form::text('email', null, ['class' => 'profile_update_right_input_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Upload Image</h1></div>
    <div class="profile_update_right_field">
        <div class="profile_update_right_browse_area">
            {!! Form::file('image', ['class' => 'profile_update_browse_style', 'accept' => 'image/*']) !!}
        </div>
    </div>
</div>

<input type="submit" value="SAVE" class="save_but1" />