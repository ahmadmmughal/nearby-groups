@extends('master')

@section('jumbotron')
    @include('partials._jumbotron-sm')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">

            @include('profile._info')

            <div class="profile_update_right_area2">
                @if( !$user->groups_joined_as_user->isEmpty() )
                    <div class="group_admin_heading"><h1>Groups I joined</h1></div>

                    <div class="group_ijoin_box2_main_area">

                        @foreach( $user->groups_joined_as_user as $group )
                            <div class="group_ijoin_box2">
                                <div class="group_ijoin_box1_img_area"><img src="{{ $group->thumbnail_url }}" alt="" /></div>
                                <h2><a href="{{ route('group.show', ['group' => $group->id]) }}">{{ $group->name }}</a></h2>
                                <h3>Category: <span>{{ $group->category }}</span></h3>
                                <p>{{ $group->description }}</p>
                                <div class="group_ijoin_box2_bottom_text_area">
                                    <h4><a href="#">Report Abuse</a>   |   <a href="{{ route('group.leave', ['group' => $group->id]) }}"><span>Leave</span></a>    </h4>
                                </div>
                            </div>
                        @endforeach

                    </div>
                @endif

                @if( !$user->groups_joined_as_admin->isEmpty() || !$user->groups_joined_as_moderator->isEmpty() )
                    <div class="group_admin_heading2"><h1>Groups I Administer or Moderate</h1></div>

                    <div class="group_ijoin_box2_main_area">
                        @foreach( $user->groups_joined_as_admin as $group )
                            <div class="group_ijoin_box2">
                                <div class="group_ijoin_box1_img_area"><img src="{{ $group->thumbnail_url }}" alt="" /></div>
                                <h2><a href="{{ route('group.show', ['group' => $group->id]) }}">{{ $group->name }}</a></h2>
                                <h3>Category: <span>{{ $group->category->name }}</span></h3>
                                <p>{{ $group->description }}</p>
                                <div class="group_ijoin_box2_bottom_text_area">
                                    <h4>
                                        <a href="{{ route('group.edit', ['id' => $group->id]) }}">Edit</a>
                                        @if( $group->admin->id != $user->id )
                                        |
                                            <a href="{{ route('group.leave', ['group' => $group->id]) }}"><span>Leave</span></a>
                                        @endif
                                            {{--|--}}
                                        {{--<a href="#"><span>DELETE</span></a>--}}
                                        </h4>
                                </div>
                            </div>
                        @endforeach
                        @foreach( $user->groups_joined_as_moderator as $group )
                            <div class="group_ijoin_box2">
                                <div class="group_ijoin_box1_img_area"><img src="{{ $group->thumbnail_url }}" alt="" /></div>
                                <h2><a href="{{ route('group.show', ['group' => $group->id]) }}">{{ $group->name }}</a></h2>
                                <h3>Category: <span>{{ $group->category->name }}</span></h3>
                                <p>{{ $group->description }}</p>
                                <div class="group_ijoin_box2_bottom_text_area">
                                    <h4>
                                        <a href="{{ route('group.edit', ['id' => $group->id]) }}">Edit</a>
                                        @if( $group->admin->id != $user->id )
                                            |
                                            <a href="{{ route('group.leave', ['group' => $group->id]) }}"><span>Leave</span></a>
                                        @endif
                                        {{--|--}}
                                        {{--<a href="#"><span>DELETE</span></a>--}}
                                    </h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection