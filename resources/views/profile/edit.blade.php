@extends('master')

@section('jumbotron')
    @include('partials._jumbotron-sm')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            @include('profile._info')

            <div class="profile_update_right_area2">
                {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UsersController@update', $user->id], 'files' => true]) !!}
                    @include('profile._form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection