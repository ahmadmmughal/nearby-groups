@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="profile_non_login_profile_area">
                <div class="profile_non_login_img_area"><img src="{{ $user->thumbnail_url }}" alt="" /></div>
                <div class="profile_non_login_text_area">
                    <div class="profile_non_login_text_full_w">
                        <div class="profile_non_login_text_heading">Name:</div>
                        <div class="profile_non_login_main_text">
                            <h1>{{ $user->name }}</h1>
                        </div>
                    </div>

                    <div class="profile_non_login_text_full_w">
                        <div class="profile_non_login_text_heading">About Me:</div>
                        <div class="profile_non_login_main_text">
                            <h2>{{ $user->bio }}</h2>
                        </div>
                    </div>

                    <div class="profile_non_login_text_full_w">
                        <div class="profile_non_login_text_heading">Hobbies:</div>
                        <div class="profile_non_login_main_text">
                            <h2>{{ $user->hobbies }}</h2>
                        </div>
                    </div>

                    <div class="profile_non_login_text_full_w">
                        <div class="profile_non_login_text_heading">City:</div>
                        @if ( $user->city )
                            <div class="profile_non_login_main_text">
                                <h2>{{ $user->city->name }}</h2>
                            </div>
                        @endif
                    </div>

                    <div class="profile_non_login_text_full_w">
                        <div class="profile_non_login_text_heading">country:</div>
                        @if ( $user->country )
                            <div class="profile_non_login_main_text">
                                <h2>{{ $user->country->name }}</h2>
                            </div>
                        @endif
                    </div>

                </div>
            </div>

            <div class="group_ijoin_main_area">
                <div class="heading_main_area">
                    <h1>Groups I joined</h1>
                </div>

                @if( !$user->groups->isEmpty() )
                    <div class="group_ijoin_box_main_area">
                        @foreach( $user->groups as $group )
                            <div class="group_ijoin_box1">
                                <div class="group_ijoin_box1_img_area"><img src="{{ $group->thumbnail_url }}" alt="" /></div>
                                <h2>{{ $group->name }}</h2>
                                <h3>Category: <span>{{ $group->category->name }}</span></h3>
                                <p>{{ $group->description }}</p>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection