<div class="profile_update_left_area">
    <div class="profile_img_area2"><img src="{{ $auth->thumbnail_url }}" alt="" /></div>
    {{--<a href="#" class="change_photo_but">Change Photo</a>--}}
    {{--<h1 class="status_heading">Status</h1>--}}
    {{--<select class="profile_update_left_select_style"><option>Online</option><option>Online</option></select>--}}
    <div class="profile_update_left_massage_but">
        <h3>Messages</h3>
        <ul>
            <li><a href="#login-box4" class="login-window4">Inbox</a></li>
            <li><a href="#login-box5" class="login-window5">Sent</a></li>
        </ul>
        <a href="{{ route('user.edit', ['id' => $auth->id]) }}" class="edit_profile_but">Edit Profile</a>
        <a href="{{ route('user.show_groups', ['id' => $auth->id]) }}" class="groups_join_but">My Groups</a>
        <a href="{{ url('logout') }}" class="edit_profile_but">Logout</a>
    </div>
</div>