@extends('master')

@section('jumbotron')
    @include('partials._jumbotron-sm')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="created_group_but_main_area"><a href="{{ route('group.create') }}" class="created_group_but"><img src="{{ url('images/create_icon.png') }}" alt="" /> Create Group</a></div>

            @include('profile._info')

            <div class="profile_update_right_area2">
                <div class="group_admin_heading3"><h1>Groups Feed</h1></div>

                @if( !$user->feed_posts->isEmpty() )
                    @foreach( $user->feed_posts as $post )
                        <div class="groups_feed_main_area">
                            <div class="groups_feed_left_area"><h1>{{ \Carbon\Carbon::parse($post->created_at)->toFormattedDateString() }}</h1></div>
                            <div class="groups_feed_right_area">
{{--                                <div class="groups_feed_right_round_img_area"><img src="{{ $post->group->thumbnail_url }}" alt="" /></div>--}}
                                <div class="groups_feed_right_text_area">
                                    <h1 class="groups_feed_right_text_area_group_mane"><a href="{{ route('group.show', ['group' => $post->group->id]) }}">{{ $post->group->name }}</a></h1>
                                    <h2 class="groups_feed_right_text_area_user_name">{{ $post->user->name }}</h2>
                                    <div class="group_ijoin_box3">
                                        <div class="group_ijoin_box3_img_area"><img src="{{ $post->group->thumbnail_url }}" alt="" /></div>
                                        <h2><a href="{{ route('group.show', ['group' => $post->group->id]) }}">{{ $post->group->name }}</a></h2>
                                        <h3>Category: <span>{{ $post->group->category->name }}</span></h3>
                                        <p>{{ $post->content }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection