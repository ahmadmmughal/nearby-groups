<body>
A request has recently been made to change your password.

{!! link_to_route('password.reset', 'Reset your password now', ['id' => $id, 'code' => $code]) !!}
</body>