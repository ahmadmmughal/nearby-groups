<body>
Thank you for signing up for Nearby Groups.

{!! link_to_route('auth.verify', 'Please verify your account.', ['id' => $id, 'code' => $code]) !!}
</body>