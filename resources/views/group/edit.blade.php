@extends('master')

@section('jumbotron')
    @include('partials._jumbotron-sm')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">

            @include('group._info')

            <div class="profile_update_right_area2">
                {!! Form::model($group, ['method' => 'PATCH', 'action' => ['GroupsController@update', $group->id], 'files' => true]) !!}
                    @include('group._form')
                {!! Form::close() !!}


                    <div class="group_admin_heading2"><h1>Add / Remove Moderator</h1></div>

                {!! Form::open(['route' => ['group.remove_moderator', $group->id], 'method' => 'DELETE']) !!}
                    <div class="add_remove_moderator_box1">
                        <div class="add_remove_moderator_picture"><img src="{{ url('images/demo_profile.jpg') }}" alt="" /></div>
                        <div class="add_remove_moderator_text_area">
                            {!! Form::select('user', $group->moderators->lists('name', 'id'), null, ['class' => 'add_remove_moderator_input_style1']) !!}
                            <input type="submit" value="DELETE" class="add_remove_moderator_submit_but" />
                        </div>
                    </div>
                {!! Form::close() !!}

                {!! Form::open(['route' => ['group.add_moderator', $group->id]]) !!}
                    <div class="add_remove_moderator_box1">
                        <div class="add_remove_moderator_picture"><img src="{{ url('images/demo_profile.jpg') }}" alt="" /></div>
                        <div class="add_remove_moderator_text_area">
                            {!! Form::select('user', $group->members->lists('name', 'id'), null, ['class' => 'add_remove_moderator_input_style1']) !!}
                            <input type="submit" value="ADD" class="add_remove_moderator_submit_but" />
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection