<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Group Name</h1></div>
    <div class="profile_update_right_field">
        {!! Form::text('name', null, ['class' => 'profile_update_right_input_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Description</h1></div>
    <div class="profile_update_right_field">
        {!! Form::textarea('description', null, ['class' => 'profile_update_right_textarea_style1']) !!}
    </div>
</div>


<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Category</h1></div>
    <div class="profile_update_right_field">
        {!! Form::select('category_id', $categories, null, ['class' => 'profile_update_right_select_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>SUB Category</h1></div>
    <div class="profile_update_right_field">
        {!! Form::select('sub_category_id', $sub_categories, null, ['class' => 'profile_update_right_select_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>COUNTRY</h1></div>
    <div class="profile_update_right_field">
        {!! Form::select('country_id', $countries, null, ['class' => 'profile_update_right_select_style1']) !!}
    </div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>City</h1></div>
    <div class="profile_update_right_field">
        {!! Form::select('city_id', $cities, null, ['class' => 'profile_update_right_select_style1']) !!}</div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Locality</h1></div>
    <div class="profile_update_right_field">{!! Form::text('locality', null, ['class' => 'profile_update_right_input_style1']) !!}</div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Privacy</h1></div>
    <div class="profile_update_right_field">{!! Form::radio('privacy', PRIVATE_GROUP, null, null) !!} Private</div>
    <div class="profile_update_right_field">{!! Form::radio('privacy', PUBLIC_GROUP, true, null) !!} Public</div>
</div>

<div class="profile_update_right_area2_full_w">
    <div class="profile_update_right_heding"><h1>Upload Image</h1></div>
    <div class="profile_update_right_field"><div class="profile_update_right_browse_area">{!! Form::file('image', ['class' => 'profile_update_browse_style', 'accept' => 'image/*']) !!}</div></div>
</div>


<input type="submit" value="SAVE" class="save_but1" />