@extends('master')

@section('jumbotron')
    @include('partials._jumbotron-sm')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">

            @include('group._info')

            <div class="profile_update_right_area">
                <div class="profile_update_right_area_heading">{{ $group->name }}</div>
                <div class="grouppage_tab_cont">
                    <div id="parentHorizontalTab">
                        <ul class="resp-tabs-list hor_1">
                            <li>Feed</li>
                            <li>Description</li>
                            <li>Members</li>
                            <li>Events</li>
                        </ul>
                        <div class="resp-tabs-container hor_1">
                            <div>
                                @if( $group->users->contains('id', $auth->id) )
                                    <div class="grouppage_tab_innerbox">
                                        <div id="ChildVerticalTab_2">
                                            <ul class="resp-tabs-list ver_1">
                                                <li class="group_name_nested_tabcolor"><img src="{{ url('images/groupname_icon1.jpg') }}"  alt=""/> Update Status</li>
                                                {{--<li class="group_name_nested_tabcolor"> <img src="{{ url('images/groupname_icon2.jpg') }}"  alt=""/> Add Photo/Video</li>--}}
                                                {{--<li class="group_name_nested_tabcolor"> <img src="{{ url('images/groupname_icon3.jpg') }}"  alt=""/> Create Photo Album</li>--}}
                                            </ul>
                                            <div class="resp-tabs-container ver_1">
                                                <div>
                                                    {!! Form::open(['action' => ['PostsController@store', 'group' => $group->id], 'files' => true, 'id' => 'post-form']) !!}
                                                    {!! Form::close() !!}

                                                    <div class="group_name_yourmind_cont">
                                                        <img src="{{ $auth->thumbnail_url }}" alt="" class="group_name_yourmind_cont_image"/>
                                                        <input type="hidden" name="type" value="text" />
                                                        <textarea form="post-form" name="content" class="group_name_yourmind_cont_textarea" placeholder="What's on your mind.."></textarea>
                                                    </div>

                                                    <input type="submit" form="post-form" class="group_name_yourmind_cont_submit1" value="Post">
                                                    {{--<input type="button" class="group_name_yourmind_cont_submit2" value="Public">--}}

                                                </div>
                                                {{--<div>--}}
                                                    {{--<div class="group_name_yourmind_cont">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nibh urna, euismod ut ornare non, volutpat vel tortor. Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis urna gravida mollis.</div>--}}
                                                {{--</div>--}}
                                                {{--<div>--}}
                                                    {{--<div class="group_name_yourmind_cont">--}}
                                                        {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nibh urna, euismod ut ornare non, volutpat vel tortor. Integer laoreet placerat suscipit.--}}
                                                    {{--</div>--}}

                                                    {{--<input type="button" class="group_name_yourmind_cont_submit1" value="Post">--}}
                                                    {{--<input type="button" class="group_name_yourmind_cont_submit2" value="Public">--}}
                                                {{--</div>--}}

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if( !$group->posts->isEmpty() )
                                    @foreach( $group->posts->sortByDesc('created_at') as $post )
                                        <div class="grouppage_tab_innerbox">
                                            <div class="grouppage_tab_innerbox_top_panel">
                                                <img src="{{ $post->user->thumbnail_url }}" alt=""/> <a href="{{ route('user.show', ['user' => $post->user->id]) }}"><span>{{ $post->user->name }}</span></a><br>
                                                {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                                                <p style="display: block" class="grouppage_tab_innerbox_matter">{{ $post->content }}</p>
                                            </div>

                                            {{--<img src="images/group_midl_banner.jpg"  alt="" class="grouppage_tab_innerbox_banner"/>--}}

                                            @if( !$post->comments->isEmpty() )
                                                @foreach( $post->comments as $comment )
                                                    <div style="padding-left: 80px;" class="grouppage_tab_innerbox_top_panel">
                                                        <img src="{{ $comment->user->thumbnail_url }}" alt=""/> <a href="{{ route('user.show', ['user' => $comment->user->id]) }}"><span>{{ $comment->user->name }}</span></a><br>
                                                        {{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}
                                                        <p style="display: block" class="grouppage_tab_innerbox_matter">{{ $comment->text }}</p>
                                                    </div>
                                                @endforeach
                                            @endif

                                            @if ( $auth && $group->users->contains('id', $auth->id) )
                                                {!! Form::open(['action' => ['CommentsController@store', 'post' => $post->id]]) !!}
                                                    <textarea name="text" class="grouppage_tab_innerbox_bottombanner" placeholder="Write a comment..."></textarea> <input type="submit" class="group_name_yourmind_cont_submit1" value="Comment">
                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <div>
                                <p class="describtiona_matter">{{ $group->description }}</p>
                            </div>

                            <div>
                                @if( !$group->users->isEmpty() )
                                    <div class="group_member_box_cont">
                                        @foreach($group->users as $user)
                                            <div class="group_member_box">
                                                <a href="{{ route('user.show', ['user' => $user->id]) }}" style="background: none;"><img src="{{ $user->thumbnail_url }}"  alt=""/></a>
                                                <p>{{ $user->name }}</p>
                                                @if( $group->users->contains('id', $auth->id) && ($auth->id != $user->id) )
                                                    <div class="group_member_box_link_panel"><a href="#login-box6" class="login-window6" data-email="{{ $user->email }}">Send Message</a></div>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                                @if( !$group->pending_users->isEmpty() && ($group->admins->contains('id', $auth->id) || $group->moderators->contains('id', $auth->id)) )
                                    <h2>Pending Memberships</h2>

                                    <div class="group_member_box_cont">
                                        @foreach($group->pending_users as $user)
                                            <div class="group_member_box">
                                                <a href="{{ route('user.show', ['user', $user->id]) }}" style="background: none;"><img src="{{ $user->thumbnail_url }}"  alt=""/></a>
                                                <p>{{ $user->name }}</p>
                                                <span class="group_member_box_link_panel" style="float: none;"><a href="{{ route('group.accept_member', ['group' => $group->id, 'user' => $user->id]) }}" >Accept</a></span>
                                                <span class="group_member_box_link_panel" style="float: none;"><a href="{{ route('group.reject_member', ['group' => $group->id, 'user' => $user->id]) }}" >Reject</a></span>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>

                            <div>

                                @if( $group->users->contains('id', $auth->id) )
                                    <a href="#login-box8" class="login-window8"><div style="float:none; padding-left: 32px; width: 25%; margin: 0px auto; top: 10px; position: relative;" class="group_name_events_create_events_button">Create Event</div></a>
                                @endif

                                @if( !$group->events->isEmpty() )
                                    <div class="group_name_events_panel">
                                        @foreach( $group->events as $event )
                                            <div class="group_name_events_panel_colbox">
                                                {{--<h1>Upcomming Events</h1>--}}
                                                <h2>{{ $event->title }}</h2>
                                                {{ $event->description }}
                                                <p><span>{{ $event->date->toFormattedDateString() }} </span>  |  {{ $event->venue }}</p>

                                                <div class="group_name_events_panel_colbox_map">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27081.841070275368!2d115.85266200000001!3d-31.954652850000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32bad5293bd573%3A0x504f0b535df4ee0!2sPerth+WA%2C+Australia!5e0!3m2!1sen!2sin!4v1442575861107" width="100%" height="140px" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                            </div>


                            <div id="login-box8" class="login-popup8">
                                <a href="#" class="close"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
                                <!--start_popup1_main_area-->
                                <div class="popup1_main_area5">
                                    {!! Form::open(['action' => ['EventsController@store', 'group' => $group->id], 'files' => true]) !!}
                                        @include('group.event._form')
                                    {!! Form::close() !!}
                                </div>
                                <!--end_popup1_main_area-->
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            //Horizontal Tab
            $('#parentHorizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });

            $('#ChildVerticalTab_2').easyResponsiveTabs({
                type: 'default',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_1', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#FFF', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
            });


            // Child Tab
            $('#ChildVerticalTab_1').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_1', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
            });



            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
@endsection