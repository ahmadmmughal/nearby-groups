@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="founded_page_left_area">
                <div class="active_group_img_area2"><img src="{{ $group->thumbnail_url }}" alt="" /></div>
                <h1>Group Name</h1>
                <p>{{ $group->name }}</p>
                <h1>Founded on</h1>
                <p>{{ \Carbon\Carbon::parse($group->created_at)->toFormattedDateString() }}</p>
                <h1>Managed By</h1>
                <div class="profile_img_area"><img src="{{ $group->admin->thumbnail_url }}" alt="" /></div>
                <p>{{ $group->admin->name }}</p>
                <h1>Available</h1>
            </div>

            <div class="founded_page_right_area">
                <h1>{{ $group->name }}</h1>
                <h2>Description</h2>
                <p>{{ $group->description }}</p>
                <a href="#login-box2" class="signup_but2 login-window2"><img src="{{ url('images/login_but.png') }}" alt="" /> Already Member? Login</a>
                <a href="#login-box" class="login_but2 login-window"><img src="{{ url('images/sign_up_but.png') }}" alt="" /> New Member? Signup</a>

            </div>
        </div>
    </div>
@endsection