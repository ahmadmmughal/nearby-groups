<div class="popup1_form_area3">
    {!! Form::text('title', null, ['class' => 'popup1_input_style1', 'placeholder' => 'Title']) !!}
    {!! Form::textarea('description', null, ['class' => 'textarea_input_style1', 'placeholder' => 'Description']) !!}
    {!! Form::text('date', null, ['class' => 'popup1_input_style1', 'placeholder' => 'Date', 'id' => 'datepicker']) !!}
    {!! Form::text('venue', null, ['class' => 'popup1_input_style1', 'placeholder' => 'Venue']) !!}
</div>

<div class="popup1_map_main_area">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27081.948426183226!2d115.86819735461428!3d-31.954288726641984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32bad5293bd573%3A0x504f0b535df4ee0!2sPerth+WA%2C+Australia!5e0!3m2!1sen!2sin!4v1442571818312" width="100%" height="140px;" frameborder="0" style="border:0" allowfullscreen></iframe>

    <input type="submit" value="CREATE" class="popup1_create_but" />
</div>

@section('javascripts')

@endsection