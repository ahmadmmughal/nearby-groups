<div class="profile_update_left_area2">

    <div class="grouppage_squre_image_cont"><img src="{{ $group->thumbnail_url }}" alt="" class="grouppage_squre_image"/></div>

    <h1 class="status_heading">Group Name</h1>
    <div class="grouppage_title">{{ $group->name }}</div>
    <h1 class="status_heading">Founded on</h1>
    <div class="grouppage_title">{{ \Carbon\Carbon::parse($group->created_at)->toFormattedDateString() }}</div>

    <h1 class="status_heading">Admin</h1><br>
    <div class="profile_img_area2"><a href="{{ route('user.show', ['id' => $group->admin->id]) }}"><img src="{{ $group->admin->thumbnail_url }}" alt="" /></a></div>
    <div class="grouppage_title"><a href="{{ route('user.show', ['id' => $group->admin->id]) }}">{{ $group->admin->name }}</a></div>

    @if( !$group->moderators->isEmpty() )
        <h1 class="status_heading">Moderators</h1><br>

        @foreach( $group->moderators as $moderator )
            <div class="profile_img_area2"><a href="{{ route('user.show', ['id' => $moderator->id]) }}"><img src="{{ $moderator->thumbnail_url }}" alt="" /></a></div>
            <div class="grouppage_title"><a href="{{ route('user.show', ['id' => $moderator->id]) }}">{{ $moderator->name }}</a></div>
        @endforeach
    @endif

    @if ( $auth )
        @if ( $group->all_users->contains('id', $auth->id) )
            @if( $group->users->contains('id', $auth->id) )
                @if($group->admin->id != $auth->id)
                    <a href="{{ route('group.leave', ['group' => $group->id]) }}" class="edit_profile_but">Leave Group</a>
                @endif
            @else
                <a href="{{ route('group.leave', ['group' => $group->id]) }}" class="edit_profile_but">Cancel Request</a>
            @endif
        @else
            <a href="{{ route('group.join', ['group' => $group->id]) }}" class="groups_join_but">Join Group</a>
        @endif
    @endif
</div>