@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="contact_main_area">
                <p>
                    <a href="https://www.facebook.com/nearbygroups" target="_blank"><img src="{{ url('images/siacial_icon1.gif') }}" alt="" /></a>
                    <a href="https://twitter.com/nearbygroups" target="_blank"><img src="{{ url('images/siacial_icon2.gif') }}" alt="" /></a>
                    <a href="https://plus.google.com/114311441177987351573/about" target="_blank"><img src="{{ url('images/siacial_icon3.gif') }}" alt="" /></a>
                    <a href="https://www.youtube.com/channel/UCv69F26WuBeztwk33ITSQvQ" target="_blank"><img src="{{ url('images/siacial_icon4.gif') }}" alt="" /></a>
                </p>
                <a href="https://nearbygroups.freshdesk.com/support/home" class="contact_help_but">Help Center</a>
            </div>
        </div>
    </div>
@endsection