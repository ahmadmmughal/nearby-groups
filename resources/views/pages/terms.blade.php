@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="heading_main_area">
                <h1>Nearby Groups - Terms of Service</h1>
            </div>
            <div class="terms_page_main_area">
                <br/>
                <h2>THE FOLLOWING DESCRIBES THE TERMS ON WHICH NEARBY GROUPS OFFERS YOU ACCESS TO OUR WEBSITES, PLATFORM AND SERVICES:</h2>
                <br/>
                Welcome to NearbyGroups.com. We provide our websites platforms to you subject to the terms of service set forth in this Terms of Service Agreement (the "Agreement").
                <br/>
                <br/>
                We may update the Agreement at any time. You can view the most recent version at http://www.nearbygroups.com/terms/. It is your responsibility to review the most recent version of the Agreement frequently and remain informed about any changes to it. By continuing to use the Platform, you consent to any updates to this Agreement. This version of the Agreement supersedes all earlier versions, and comprises the entire agreement between you and Nearby Groups regarding the platform. By accessing or using the platform, you accept this Agreement and any modifications that we may make to this Agreement from time to time. If you do not agree to any provision of this Agreement, you should not use the platform.
                <br/><br/>
                <h1>1. Platform</h1>
                <br/>
                Our platform provides our users with a variety of resources to facilitate organizing of groups.
                <br/><br/>
                You agree to comply with all applicable domestic and international laws, statutes, ordinances and regulations regarding your use of our Platform, including without limitation your participation in or involvement with any Nearby Group's Group. You also agree to comply with all applicable laws, statutes, ordinances and regulations regarding the transmission of technical data exported from the India or the country in which you reside.
                <br/><br/>
                <h1>2. Membership</h1>
                <br/>
                2.1 Eligibility. Our platform is available to all individuals who are at least 18 years of age. By registering to use our platform, you represent and warrant that you are at least 18 years of age. Additional eligibility requirements for a particular Nearby Group's Group may be set by the organizer, co-organizer, or assistant organizer of the group.
                <br/><br/>
                We have the right, in our sole discretion, to suspend or terminate your use of our platform and refuse any and all current or future use of all or any portion of our platform.
                <br/><br/>
                2.2 Password and Security. When you complete our registration process you will create a password that will enable you to access our platform. You agree to maintain the confidentiality of your password, and are fully responsible for all liability and damages resulting from your failure to maintain that confidentiality and all activities that occur through the use of your password. You agree to immediately notify us of any unauthorized use of your password or any other breach of security. You agree that Nearby Groups cannot and will not be liable for any loss or damage arising from your failure to comply with this Section.
                <br/><br/>
                <h1>3. Your Information</h1>
                <br/>
                3.1 Definition. "Your Information" is defined as any information post or other material you provide (directly or indirectly), including through the registration process for a Group, or through the use of our platform, in any public message board (including the personal introduction section of each topic group) or through email. You are solely responsible for Your Information, and we act as a passive conduit for your online distribution and publication of your Public Information (as defined below).
                <br/><br/>
                Any of Your Information that, through the use of our platform or otherwise, you submit or make available for inclusion on publicly accessible areas of our website is referred to as "Public Information"; any other portion of Your Information shall be referred to as "Private Information." "Publicly accessible" areas of our website are those areas that are available either to some or all of our members (i.e., not restricted to your viewing only) or to the general public.
                <br/><br/>
                You should understand that your Public Information may be accessible by and made public through syndication programs (including data feed tools) and by search engines, metasearch tools, crawlers, metacrawlers and other similar programs.
                <br/>
                3.2 Restrictions. In consideration of your use of our platform, you agree that Your Information:
                <br/>
                &middot;	(a) shall not be fraudulent;
                <br/>
                &middot;	(b) shall not infringe any third party's copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy;
                <br/>
                &middot;	(c) shall not violate any law, statute, ordinance or regulation;
                <br/>
                &middot;	(d) shall not be defamatory, trade libelous, unlawfully threatening or unlawfully harassing;
                <br/>
                &middot;	(e) shall not be obscene or contain, pornography, child pornography, or photographs of unclothed person(s);
                <br/>
                &middot;	(f) shall not contain any viruses, Trojan horses, worms, time bombs, cancelbots, easter eggs or other computer programming routines that may damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information;
                <br/>
                &middot;	(g) shall not create liability for us or cause us to lose (in whole or in part) the services of our ISPs or other suppliers;
                <br/>
                &middot;	(h) shall not link directly or indirectly to any materials to which you do not have a right to link to or include.
                In addition, you agree that you will provide us with your valid, current email address, both at the time of your registration with us and from time to time as your email address changes.
                <br/>
                3.3 License. We do not claim ownership of Your Information. We will use Your Information only in accordance with our privacy policy. However, to enable us to use your Public Information and to ensure we do not violate any rights you may have in your Public Information, you grant Nearby Groups a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sublicensable (through multiple tiers) right to exercise, commercialize and exploit the copyright, publicity, and database rights (but no other rights) you have in your Public Information, in any media now known or not currently known, with respect to your Public Information.
                <br/>
                3.4 Restriction on Use of Your Information. Except as otherwise provided in our privacy policy, we will not sell, rent or otherwise disclose any of your Personally Identifiable Information (as defined in our privacy policy) about you (including your email address) to any third party.
                <br/><br/>
                <h1>4. Use of Platform</h1>
                <br/>
                4.1 Control. You, and not Nearby Groups, are entirely responsible for all of your Public Information that you upload, post, email, transmit or otherwise make available via our Platform. We do not control your Public Information or the Public Information of or posted by other users and do not guarantee the accuracy, integrity or quality of Your Information or the Information of or posted by other users. You understand that by using our platform, you may be exposed to information that is offensive, indecent or objectionable. We do not have any obligation to monitor, nor do we take responsibility for, Your Information, Public Information or information of or posted by other users. You agree that under no circumstances will Nearby Groups its directors, officers, shareholders, employees, consultants, agents, advisers, affiliates, subsidiaries or its third-party partners be liable in any way for any information, including, but not limited to, for any errors or omissions in Your Information or the Information of or posted by other users, or for any loss or damage of any kind incurred as a result of the use of Your Information or Information of or posted by other users posted, emailed, transmitted or otherwise made available in connection with our platform, or for any failure to correct or remove information.
                <br/><br/>
                4.2 Grounds for Removal, Sanction and/or Suspension. Notwithstanding any other provision of this Agreement, the following types of actions are cause for immediate removal, repeal and/or suspension or termination of your account:
                <br/><br/>
                (a) The use of our platform to (including, without limitation, eligibility requirements):
                <br/><br/>
                &middot;	(i) harm or intimidate another person in any way, including restricting or inhibiting any other user from using our Platform;
                <br/>
                &middot;	(ii) impersonate any person or entity (including Nearby Groups, Nearby Groups staff and other members), or falsely state or otherwise misrepresent your affiliation with any person, through the use of similar email addresses, nicknames, or creation of false account(s) or any other method or device;
                <br/>
                &middot;	(iii) disguise the origin of any Public Information that is transmitted to any third party;
                <br/>
                &middot;	(iv) "stalk" or otherwise harass another;
                <br/>
                &middot;	(v) advertise merchandise, auctions, services or commercial websites, including offers to trade or charitable solicitations unrelated to the topic or spirit of the Nearby Group's Group;
                <br/>
                &middot;	(vi) resell Public Information or access to Public Information;
                <br/>
                &middot;	or (vii) collect or store personal data about other users;
                <br/><br/>
                (b) Posting any Public Information or other material:
                <br/><br/>
                &middot;	(i) that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, intimidating, vulgar, obscene, profane, libelous, invasive of another's privacy (including the posting of private emails or contact information about another individual), hateful, or racially, ethically or otherwise objectionable, including any Public Information or other material that may be considered hate speech;
                <br/><br/>
                &middot;	(ii) that is obscene, pornographic or adult in nature;
                <br/>
                &middot;	(iii) that you do not have a right to make available under any law or under contractual or fiduciary relationships;
                <br/>
                &middot;	(iv) that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party or rights of publicity or privacy;
                <br/>
                &middot;	(v) that is unsolicited or unauthorized advertising, promotional materials, or any other form of solicitation (including, but not limited to, "spam," "junk mail," and "chain letters");
                <br/>
                &middot;	(vi) that is inappropriate, posted in bad faith, or contrary to the spirit of any Nearby Group's Group
                <br/>
                &middot;	(vii) that uses the Platform primarily as a lead generator or listing service for another website;
                <br/><br/>
                (c) Encouraging others to violate this Agreement;
                <br/><br/>
                (d) Refusing to follow Nearby Groups staff instruction or direction;
                <br/><br/>
                (e) Violation (intentional or unintentional) of this Agreement, or of any applicable local, state, national or international law, statute, ordinance or regulation;
                <br/><br/>
                (f) Disclose the Private Information of any member of a Nearby Group without the permission of that member; or
                <br/><br/>
                (g) Transmit money to Nearby Groups or any Organizer or Creator or Host through financial accounts that are stolen, fraudulent or otherwise unauthorized.
                <br/><br/>
                Also, your posting of other inappropriate actions, Public Information or other materials may also warrant removal and/or suspension from our website. Nearby Groups reserves the right to remove any post or other material without warning or further notice.
                <br/><br/>
                While we prohibit such conduct and content, you understand and agree that you nonetheless may be exposed to such conduct or content and that you use the platform and attend meetings at your own risk.
                <br/><br/>
                For purposes of this Agreement, "posting" includes uploading, posting, emailing, transmitting or otherwise making available. Without limiting the foregoing, Nearby Groups and its designees shall have the right to remove any Public Information or other material that violates this Agreement or is otherwise objectionable.
                <br/><br/>
                4.3 Interference with Platform.
                <br/>
                You agree that you will not:
                <br/><br/>
                (a) upload, post, email, or otherwise transmit any computer routines, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;
                <br/><br/>
                (b) interfere with or disrupt our platform or networks connected to our website or through the use of our platform, or disobey any requirements, procedures, policies or regulations of networks connected to our website or through the use of our platform, or otherwise interfere with our platform in any way, including through the use of JavaScript, active or other coding;
                <br/><br/>
                (c) take any action that imposes an unreasonable or disproportionately large load on our infrastructure; or
                <br/><br/>
                (d) copy, reproduce, alter, modify, or publicly display any information displayed on our website (except for Your Information), or create derivative works from our website (other than from Your Information), to the extent that such action(s) would constitute copyright infringement or otherwise violate the intellectual property rights of Nearby Groups or any other third party, except with the prior written consent of Nearby Groups or the appropriate third party.
                <br/><br/>
                4.4 General Practices Regarding Use of Platform. You acknowledge and agree that we may establish general practices and limits concerning the use of our platform. You agree that we have no responsibility or liability for the storage or the deletion of, or the failure to store or delete, any of Your Information. You acknowledge that we reserve the right to log off users who are inactive for an extended period of time. In addition, you acknowledge that we reserve the right to change these general practices and limits at any time, in our sole discretion, with or without notice.
                <br/><br/>
                <h1>5. Meetings at Venues</h1>
                <br/>
                5.1 Nearby Groups Meetings. Through our platform we provide tools that enable our users to arrange physical meetings at venues that include, but are not limited to, public parks, private homes or private enterprises (such as coffee shops or retail stores). We do not supervise these meetings and are not involved in any way with the actions of any individuals at these meetings. As a result, we have no control over the identity or actions of the individuals who are present there and we request that our users exercise caution and good judgment when attending these meetings.
                <br/><br/>
                5.2 Release. Because we do not supervise or control the meetings or interactions among or between members and because we are not involved in any way with physical transportation to or from meetings or with the actions of any individuals and because we have very limited control, if any, over the quality, safety, morality, legality, truthfulness or accuracy of various aspects of the platform you agree that you bear all risk and you agree to release us (and our officers, directors, shareholders, agents, employees, affiliates, subsidiaries, and third party partners) and Organizers and Creators or Hosts and their designees from claims, demands, and damages (actual and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, now and in the future, arising out of or in any way connected with your use of the platform, your Third Party Transactions, our resolution of any disputes among users, and/or your transportation to or from, attendance at, or the actions of you or other persons at, a meeting. You further waive any and all rights and benefits otherwise conferred by any statutory or non-statutory law of any jurisdiction that would purport to limit the scope of a release or waiver.
                <br/><br/>
                <h1>6. Communications from Nearby Groups and Members of the Nearby Groups Community</h1>
                <br/>
                6.1 Nearby Groups Communications. You understand that certain communications, such as Nearby Groups service announcements and newsletters, as well as offers of sponsorship or promotion relevant and beneficial to you or your group, are part of our platform. By using our platform, you expressly agree to receive such communications from Nearby Groups.
                <br/><br/>
                6.2 Communications with Members of the Nearby Groups Community.
                <br/><br/>
                &middot;	Your Organizer or Creator or Host. By joining a Nearby Groups, you understand and agree that you may receive communication from your Organizer or Creator or Host and their designees in the normal course of utilizing our platform. Your Organizer's or Creator's or Host's messages will be relayed to your email address through our Platform, which does not disclose your email address.
                <br/>
                &middot;	Nearby Groups Group Communication. If you are an Organizer or Creator or Host, you agree to receive messages from individual members of your Nearby Groups community.

                <br/><br/>
                6.4 Use of Pop-up Windows. Nearby Groups will not launch pop-up windows to advertise third-party products or services.
                <br/><br/>
                6.5 Other Users. We do not control the information provided by other users, which is made available through our system. You may find other users' information to be offensive, harmful, inaccurate or deceptive. Please use caution and common sense when using our website. Please note there is a risk that you may be dealing with underage persons or people acting under false pretense.
                <br/><br/>
                <h1>7. Privacy</h1>
                <br/>
                Nearby Groups collects registration and other information about you through the platform. Our collection, use, and disclosure of this information is governed by the Nearby Groups Privacy Policy Statement available at http://www.nearby groups.com/privacy/.
                <br/><br/>
                <h1>8. Links</h1>
                <br/>
                We may provide, or third parties may provide, links to other websites or resources. Because we have no control over such websites or resources, you acknowledge and agree that we are not responsible for the availability of such websites or resources, and do not endorse and are not responsible or liable for any content, advertising, products, or other materials on or available from such websites or resources. You also acknowledge and agree that Nearby Groups shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, goods or services available on or through any such websites or resource.
                <br/>
                <br/>
                <h1>9. Indemnity</h1>
                <br/>
                You agree to indemnify and hold us and our officers, directors, shareholders, agents, employees, consultants, affiliates, subsidiaries and third-party partners harmless from any claim or demand, including reasonable attorneys' fees, made by any third party due to or arising out of your breach of your representations and warranties or this Agreement or the documents it incorporates by reference, your use of our platform, Your Information, your violation of any law, statute, ordinance or regulation or the rights of a third party, your participation in a group, or your participation as an Organizer or Creator or Host or in group whether the claim or demand is due to or arising out of your transportation to or from, attendance at, or the actions of you or other users at Nearby Groups Gatherings). Without limiting the foregoing, you, as an Organizer or Creator or Host, agree to indemnify and hold us and our officers, directors, shareholders, agents, employees, consultants, affiliates, subsidiaries and third-party partners harmless from any claim or demand, including reasonable attorneys' fees, made by any Nearby Groups Group or third party due to or arising out of your actions as an Organizer or Creator or Host, including your use of money paid to you by members of your Nearby Groups Group.
                <br/><br/>
                <h1>10. Modifications</h1>
                <br/>
                We reserve the right at any time or times to modify or discontinue, temporarily or permanently, all or any portion of our platform with or without notice. You agree that we shall not be liable to you or to any third party for any modification, suspension or termination of our platform.
                <br/><br/>
                <h1>11. Termination; Breach</h1>
                <br/>
                You agree that we, in our sole discretion, may issue a warning, temporarily suspend, indefinitely suspend, remove content or information you have posted, or terminate your account your status as a particular Nearby Groups or Organizer or Creator or Host, or your ability to use all or any portion of our platform (including any APIs), for any reason, including, without limitation, (a) for lack of use, (b) if we believe that you have violated or acted inconsistently with the letter or spirit of this Agreement or the documents or agreements it incorporates by reference, (c) if we are unable to verify or authenticate any information you provide to us, or (d) if we believe that your actions may cause legal liability for you, our users or us. You agree that any termination of your account or access to all or any portion of the platform under any provision of this Agreement may be effected without prior notice, and acknowledge and agree that we may immediately deactivate or delete your account and all related information and files in your account and/or bar any further access to such files or our platform. You also agree that we shall not be liable to you or any third party for any termination of your use of or access to all or any portion of the platform.
                <br/><br/>
                <h1>12. No Resale</h1>
                <br/>
                You agree not to reproduce, duplicate, copy, sell, resell or exploit for any commercial purposes, any portion of Nearby Groups's platform, use of the platform, or access to the platform for any sales of goods or services, or promotion of a company, good, or service unrelated to the topic or spirit or the Nearby Groups Group.
                <br/><br/>
                <h1>13. Disclosures; Violations</h1>
                <br/>
                The platform offered under this Agreement is offered by Nearby Groups. Please report any violations of this Agreement by sending a notice of the violation to Nearby Groups by email, as follows:
                <br/><br/>
                connect@nearbygroups.com
                <br/><br/>
                * * *
                <br/><br/>
                By indicating during registration that you have read and agreed to this Agreement, you are agreeing that you have read and understand this Agreement and agree to all of the terms of this Agreement, which provides that all disputes, claims or controversies arising out of or relating to this Agreement shall first be dealt with through negotiation and mediation and if the dispute is not resolved shall then be submitted to binding, neutral arbitration.

            </div>
        </div>
    </div>
@endsection