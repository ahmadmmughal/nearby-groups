@extends('master')

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <form action="{{ action('AuthController@completePasswordReset', ['id' => $id, 'code' => $code]) }}" method="post">
                    {!! Form::label('password', 'Password:') !!}
                    {!! Form::password('password', ['class' => '']) !!}

                    {!! Form::label('password_confirmation', 'Confirm Password:') !!}
                    {!! Form::password('password_confirmation', ['class' => '']) !!}

                    {!! Form::button('Reset Password', ['class' => '', 'type' => 'submit']) !!}
            </form>
        </div>
    </div>
@endsection