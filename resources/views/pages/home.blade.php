@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        @if( !$active_groups->isEmpty() )
            <div class="contanear">
                <div class="heading_main_area">
                    <h1>{{ $heading }}</h1>
                </div>

                @foreach( $active_groups as $group )
                    <div class="active_group_main_area">
                        <div class="active_group_img_area">
                            <img src="{{ $group->thumbnail_url }}" alt="" />
                        </div>

                        <div class="active_group_text_area">
                            <h1><a href="{{ route('group.show', ['id' => $group->id]) }}">{{ $group->name }}</a></h1>
                            <h2>Category: <span>{{ $group->category->name }}</span></h2>
                            <p>{{ $group->description }}</p>
                            @if(Sentinel::guest())
                                <a href="#login-box2" class="login_to_join_but login-window2"><img src="{{ url('images/login_to_join_icon.png') }}" alt="" /> Login to Join</a>
                            @else
                                @if( $auth->groups->contains('id', $group->id) )
                                    <a class="login_to_join_but">Joined</a>
                                @elseif ( $auth->pending_groups->contains('id', $group->id) )
                                    <a class="login_to_join_but">Pending</a>
                                @else
                                    <a href="{{ route('group.join', ['group' => $group->id]) }}" class="login_to_join_but"><img src="{{ url('images/login_to_join_icon.png') }}" alt="" /> Join</a>
                                @endif
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection