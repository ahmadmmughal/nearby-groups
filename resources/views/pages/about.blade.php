@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="heading_main_area">
                <h1>About Us</h1>
            </div>

            <div class="terms_page_main_area">
                <p>Nearby Groups was ideated in India and turned into reality by our team spread across 3 countries. Nearby Groups is all about creating local groups and organize face-to-face meeting with people who have same interest/hobbies.
                    <br/>
                    <br/>
                    We believe that real world is far more beautiful and social compared to virtual world. <br/>
                    Learn more on our - <a href="{{ url('contact') }}">contact page</a>.<br/>
                    <br/>
                    Nearby Groups is the part of <a href="http://kmgtechlabs.com" target="_blank">kmg TechLabs</a> network.
                </p>
            </div>

            {{--<div class="our_team_left_area">--}}
                {{--<h1>Our Team</h1>--}}
                {{--<div class="our_team_box_main_area">--}}
                    {{--<div class="our_team_box1">--}}
                        {{--<div class="active_group_img_area3"><img src="images/team_icon1.jpg" alt="" /></div>--}}
                        {{--<h2>Name here</h2>--}}
                        {{--<h3>Your role goes here</h3>--}}
                        {{--<h4>Age 31 years</h4>--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                    {{--</div>--}}
                    {{--<div class="our_team_box1">--}}
                        {{--<div class="active_group_img_area3"><img src="images/team_icon2.jpg" alt="" /></div>--}}
                        {{--<h2>Name here</h2>--}}
                        {{--<h3>Your role goes here</h3>--}}
                        {{--<h4>Age 25 years</h4>--}}
                        {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="our_team_box1">--}}
                    {{--<div class="active_group_img_area3"><img src="images/team_icon3.jpg" alt="" /></div>--}}
                    {{--<h2>Name here</h2>--}}
                    {{--<h3>Your role goes here</h3>--}}
                    {{--<h4>Age 31 years</h4>--}}
                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                {{--</div>--}}
                {{--<div class="our_team_box1">--}}
                    {{--<div class="active_group_img_area3"><img src="images/team_icon4.jpg" alt="" /></div>--}}
                    {{--<h2>Name here</h2>--}}
                    {{--<h3>Your role goes here</h3>--}}
                    {{--<h4>Age 25 years</h4>--}}
                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                {{--</div>--}}

                {{--<div class="our_team_box1">--}}
                    {{--<div class="active_group_img_area3"><img src="images/team_icon5.jpg" alt="" /></div>--}}
                    {{--<h2>Name here</h2>--}}
                    {{--<h3>Your role goes here</h3>--}}
                    {{--<h4>Age 25 years</h4>--}}
                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                {{--</div>--}}

                {{--<div class="our_team_box1">--}}
                    {{--<div class="active_group_img_area3"><img src="images/team_icon6.jpg" alt="" /></div>--}}
                    {{--<h2>Name here</h2>--}}
                    {{--<h3>Your role goes here</h3>--}}
                    {{--<h4>Age 25 years</h4>--}}
                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="our_team_right_area">
                <h1>sPECIAL <span>Thanks</span></h1>
                <div class="our_team_right_heading_area">
                    <h2>Role</h2><h2>name</h2>
                </div>
                <div class="our_team_right_text_area">
                    <div class="our_team_right_text_full_w">
                        <p>Designer</p>
                        <h3><a href="http://www.logocreatorteam.com/">Bikash Bagh</a></h3>
                    </div>

                    <div class="our_team_right_text_full_w">
                        <p>Designer</p>
                        <h3><a href="https://www.freelancer.com/u/euwyng.html">Euwyn G</a></h3>
                    </div>

                    <div class="our_team_right_text_full_w">
                        <p>Developer</p>
                        <h3><a href="http://octavesolutions.co">Octave Solutions</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection