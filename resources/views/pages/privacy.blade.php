@extends('master')

@section('jumbotron')
    @include('partials._jumbotron')
@endsection

@section('content')
    <div class="body_contanear">
        <div class="contanear">
            <div class="heading_main_area">
                <h1>Nearby Groups Privacy Policy</h1>
            </div>
            <div class="terms_page_main_area">
                <br/><br/>
                Protecting your privacy is a serious matter and doing so is very important to us. Please read this Privacy Policy statement (the "Policy") to learn more about our Privacy Policy. This Policy describes the information we collect from you and what may happen to that information, and only applies to such information. This Policy applies to all sites under the NearbyGroups.com domain.
                <br/><br/>
                <h1>1. Information Collection</h1>
                <br/>
                &middot;	1.1. We collect the following information about you and your use of our Website, platform and service (together, our "platform") in order to create a better, more personalized experience for you:
                <br/>
                &middot;	1.1.1. a name selected by you or assigned by us, your email address, your name (if provided) a password selected by you, your zip code; and
                <br/>
                &middot;	1.1.2. for each Nearby Groups group in which you participate, you may choose to create and store a short description or statement which will be viewed by anyone who is accessing that group, and your message board postings.
                <br/>
                &middot;	1.1.3. for purposes of this Policy, "Personally Identifiable Information" means information that could be used to identify your personally (such as your email address or IP address), which has not been previously or subsequently disclosed by you on the public areas of our website or in messages you send to your group.
                <br/>
                &middot;	1.2. We automatically track certain basic information about our members (such as internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, click patterns, etc.). We use this information to do internal research on our members' usage patterns, demographics, interests and general behaviour to better understand and serve you and our community.
                <br/>
                &middot;	1.3. We currently contract with online partners to help manage and optimize our business and communications. We use the services of a site analytics partner to help us measure the effectiveness of our advertising and how visitors use our site. To do this, we use web beacons and cookies provided by our site analytics partner on this site. A "cookie" is a piece of data stored on your computer that is tied to information about you. The type of information collected includes the URL you came from and go to, your browser information, and IP address, and helps us learn how to improve our service. No information shared with our site analytics partner is directly linked to your Personally Identifiable Information. If you wish to opt-out of these website analysis tools, you may do so here.
                <br/>
                &middot;	1.4. For our members' convenience, we also use "cookies" to allow you to enter your password less frequently during a session and to provide for an easier registration process. If you configure your browser or otherwise choose to reject the cookies, you may still use our site. However, to best experience our website and platform and most easily use our platform you must have cookies enabled.
                <br/>
                &middot;	1.5. We may collect information such as postings you make on the public areas of our website, messages you send and correspondence we receive from other members or third parties about your activities or postings on our website.
                <br/><br/>
                <h1>2. Use of Information</h1>
                <br/><br/>
                &middot;	2.1. We use the information we collect about you (including your Personally Identifiable Information) to create a better, more personalized experience for you based on your individual usage habits, improve our marketing and promotional efforts, analyze site usage, improve our content and product offerings, and customize our site's content, layout and Services. These uses improve our site and allow us to better customize it to meet your needs. We also use the information we collect about you to resolve disputes, troubleshoot problems, and enforce our Terms of Service Agreement.
                <br/>
                &middot;	2.2. We may compile the information we collect about you and use it, in an aggregate form only, in the negotiation and establishment of service agreements with public and/or private enterprises under which such enterprises will serve as Nearby Groups partners or as venues for meetings between our members ("Nearby Groups").
                <br/>
                &middot;	2.3. We may use for promotional, sales or any use that we consider appropriate your correspondence with us or photographs submitted for publication on our website, be it via email, postings on our website, or feedback via the member polls.
                <br/><br/>
                <h1>3. Disclosure of Your Information</h1>
                <br/><br/>
                &middot;	3.1. Opt-in requirement. WITHOUT YOUR AFFIRMATIVE CONSENT (ON A CASE-BY-CASE BASIS), WE DO NOT SELL, RENT OR OTHERWISE SHARE YOUR PERSONALLY IDENTIFIABLE INFORMATION WITH OTHER THIRD PARTIES, UNLESS OTHERWISE REQUIRED AS DESCRIBED BELOW UNDER "REQUIRED DISCLOSURES". TO THE EXTENT WE SHARE INFORMATION WITH OUR PARTNERS AND ADVERTISERS, WE SHARE ONLY AGGREGATED OR OTHERWISE NON-PERSONALLY IDENTIFIABLE INFORMATION THAT IS NOT LINKED TO YOUR PERSONALLY IDENTIFIABLE INFORMATION.
                <br/>
                &middot;	3.2. You should understand that information you provide through the registration process or post to the public areas of our website, or through the use of our platform (including your name (if provided) and location information) may be accessible by and made public through syndication programs and by search engines, metasearch tools, crawlers, metacrawlers and other similar programs.
                <br/>
                &middot;	3.3. Required disclosures. Though we make every effort to preserve member privacy, we may need to disclose your Personally Identifiable Information when required by law or if we have a good-faith belief that such action is necessary to (a) comply with a current judicial proceeding, a court order or legal process served on our website, (b) enforce this Policy or the Terms of Service Agreement, (c) respond to claims that your Personal Information violates the rights of third parties; or (d) protect the rights, property or personal safety of Nearby Groups, its members and the public. You authorize us to disclose any information about you to law enforcement or other government officials as we, in our sole discretion, believe necessary or appropriate, in connection with an investigation of fraud, intellectual property infringements, or other activity that is illegal or may expose us or you to legal liability.
                <br/><br/>
                <h1>4. Reviewing, Updating, Deleting and Deactivating Personal Information</h1>
                <br/><br/>
                &middot;	4.1. After registration for our Platform and for specific topic groups, Nearby Groups Groups or Nearby Groups Everywheres, we provide a way to update your Personally Identifiable Information. Upon your request, we will deactivate your account and remove your Personally Identifiable Information from our active databases. To make this request, email connect@nearbygroups.com. Upon our receipt of your request, we will deactivate your account and remove your Personally Identifiable Information as soon as reasonably possible in accordance with our deactivation policy and applicable law. Nonetheless, we will retain in our files information you may have requested us to remove if, in our discretion, retention of the information is necessary to resolve disputes, troubleshoot problems or to enforce the Terms of Service Agreement. Furthermore, your information is never completely removed from our databases due to technical and legal constraints (for example, we will not remove your information from our back up storage).
                <br/><br/>
                <h1>5. Notification of Changes</h1>
                <br/><br/>
                &middot;	5.1. If we decide to change this Policy, we will post those changes on http://www.nearbygroups.com/privacy or post a notice of the changes to this Policy on the homepage (http://www.nearbygroups.com/) and other places we deem appropriate, so you are always aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We will use information in accordance with the Privacy Policy statement under which the information was collected.
                <br/>
                &middot;	5.2. If we make any material changes in our privacy practices, we will post a prominent notice on our website notifying you and our other members of the change. In some cases where we post a notice we will also email you and other members who have opted to receive communications from us, notifying them of the changes in our privacy practices. However, if you have deleted/deactivated your account, then you will not be contacted, nor will your previously collected personal information be used in this new manner.
                <br/>
                &middot;	5.3. If the change to this Policy would change how your Personally Identifiable Information is treated, then the change will not apply to you without your affirmative consent. However, if after a period of thirty (30) days you have not consented to the change in the Policy, your account will be automatically suspended until such time as you may choose to consent to the Policy change. Until such consent, your personal information will be treated under the Policy terms in force when you began your membership.
                <br/>
                &middot;	5.4. Any other change to this Policy (i.e., if it does not change how we treat your Personally Identifiable Information) will become are effective after we provide you with at least thirty (30) days notice of the changes and provide notice of the changes as described above. You must notify us within this 30 day period if you do not agree to the changes to the Policy and wish to deactivate your account.
                <br/><br/>
                <h1>6. Dispute Resolution</h1>
                <br/><br/>
                &middot;	6.1. Any dispute, claim or controversy arising out of or relating to this Policy or previous Privacy Policy statements shall be resolved through negotiation, mediation and arbitration as provided under our Terms of Service Agreement.
                <br/><br/>
                <h1>7. Contact Information</h1>
                <br/><br/>
                &middot;	7.1. If members have any questions or suggestions regarding this Policy, please contact Nearby Groups using email, as follows:
                <br/><br/>
                connect@nearbygroups.com
                <br/><br/>
                This Privacy Policy is governed by the terms of the Nearby Groups Terms of Service Agreement.

            </div>
        </div>
    </div>
@endsection