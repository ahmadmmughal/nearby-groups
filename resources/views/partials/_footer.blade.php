<div class="footer_contanear">
    <div class="contanear">
        <div class="footer_text_area">
            <p><a href="{{ action('PagesController@about') }}">About</a>      |      <a href="http://blog.nearbygroups.com" target="_blank">Blog</a>      |       <a href="{{ action('PagesController@terms') }}">Terms</a>      |      <a href="{{ action('PagesController@privacy') }}">Privacy</a>       |      <a href="{{ action('PagesController@contact') }}">Contact</a></p>
            <h2>&copy; 2015 All Rights Reserved | <span>Nearby Groups</span></h2>
        </div>
    </div>
</div>