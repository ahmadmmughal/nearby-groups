<div id="login-box" class="login-popup">
    <a href="#" class="close"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
    <!--start_popup1_main_area-->
    <div class="popup1_main_area">
        <div class="popup1_top_fb_area">
            <a href="{{ action('AuthController@facebookRedirect') }}"><img src="{{ url('images/fb_but.gif') }}" alt="" /></a>
            <h1>OR</h1>
        </div>

        {!! Form::open(['action' => 'AuthController@register']) !!}
        <div class="popup1_form_area">
            <input type="text" name="name" class="popup1_input_style1" placeholder="Name" />
            <input type="text" name="email" class="popup1_input_style1" placeholder="Email" />
            <input type="password" name="password" class="popup1_input_style1" placeholder="Password" />
            <input type="password" name="confirm_password" class="popup1_input_style1" placeholder="Confirm Password" />
            <input type="submit" value="Sign Up" class="popup1_signup_but" />
        </div>
        {!! Form::close() !!}

        <div class="popup1_login_area">
            <h2>Already a member?</h2>
            <a class="popup1_login_but login-window2" href="#login-box2" onclick="$('#login-box').fadeOut(100);">Login</a>
        </div>
    </div>
    <!--end_popup1_main_area-->
</div>

<div id="login-box2" class="login-popup2">
    <a href="#" class="close"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
    <!--start_popup1_main_area-->
    <div class="popup1_main_area">
        <div class="popup1_top_fb_area">
            <a href="{{ action('AuthController@facebookRedirect') }}"><img src="{{ url('images/fb_but.gif') }}" alt="" /></a>
            <h1>OR</h1>
        </div>

        {!! Form::open(['action' => 'AuthController@login']) !!}
        <div class="popup1_form_area">
            <input type="text" name="email" class="popup1_input_style1" placeholder="Email" />
            <input type="password" name="password" class="popup1_input_style1" placeholder="Password" />
            <p><label>
                    <input type="checkbox" name="remember" value="checkbox" id="CheckboxGroup1_0">
                    Remember Me </label>
                <span><a href="#login-box3" class="login-window3">Forgot Password?</a></span>
            </p>
            <input type="submit" value="Login" class="popup1_signup_but" />
        </div>
        {!! Form::close() !!}

        <div class="popup1_login_area">
            <h2>New Member?</h2>
            <a class="popup1_login_but login-window" href="#login-box" onclick="$('#login-box2').fadeOut(100);">Sign Up</a>
        </div>
    </div>
    <!--end_popup1_main_area-->
</div>

<div id="login-box3" class="login-popup3">
    <a href="#" class="close2"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
    <!--start_popup1_main_area-->
    <div class="popup1_main_area2">
        <div class="forgot_password_top_area">
            <h1>Forgot Your Password?</h1>
            <p>Enter your email address and we"ll send a link to change your password.</p>
        </div>

        {!! Form::open(['action' => 'AuthController@requestPasswordReset']) !!}
            <div class="popup1_form_area2">
                <input type="text" class="popup1_input_style1" placeholder="Email" name="email" />

                <input type="submit" value="Login" class="popup1_signup_but2" />
            </div>
        {!! Form::close() !!}
    </div>
    <!--end_popup1_main_area-->
</div>

@if( $auth && !$auth->received_messages->isEmpty() )
    <div id="login-box4" class="login-popup4">
        <a href="#" class="close"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
        <!--start_popup1_main_area-->
        <div class="popup1_main_area3">
            @foreach( $auth->received_messages as $message )
            <a href="#login-box7" class="login-window7">
                <div class="mail_small_box_area1">
                    <div class="mail_small_box_heading_and_attach_area">
                        <h1 class="email" style="display: none;">{{ $message->sender->email }}</h1>
                        <h2>From: <span class="sender">{{ $message->sender->name }}</span></h2>
                        <h3 class="time">{{ \Carbon\Carbon::parse($message->created_at)->format('l jS \\, F h:i A') }}</h3>
                    </div>
                    <div class="mail_small_box_suject_and_text_area" style="width: 100%;">
                        <h1 class="subject">{{ $message->subject }}</h1>
                        <p class="content" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">{{ $message->content }}</p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
        <!--end_popup1_main_area-->
    </div>
@endif

@if( $auth && !$auth->sent_messages->isEmpty() )
    <div id="login-box5" class="login-popup5">
        <a href="#" class="close"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
        <!--start_popup1_main_area-->
        <div class="popup1_main_area3">
            @foreach( $auth->sent_messages as $message )
                <div class="mail_small_box_area1">
                    <div class="mail_small_box_heading_and_attach_area">
                        <h1 class="email" style="display: none;">{{ $message->recipient->email }}</h1>
                        <h2>To: <span class="sender">{{ $message->recipient->name }}</span></h2>
                        <h3 class="time">{{ \Carbon\Carbon::parse($message->created_at)->format('l jS \\, F h:i A') }}</h3>
                    </div>
                    <div class="mail_small_box_suject_and_text_area" style="width: 100%;">
                        <h1 class="subject">{{ $message->subject }}</h1>
                        <p class="content" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">{{ $message->content }}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <!--end_popup1_main_area-->
    </div>
@endif

@if( $auth )
    <div id="login-box6" class="login-popup6">
        <a href="#" class="close"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
        <!--start_popup1_main_area-->
        <div class="popup1_main_area4">
            {!! Form::open(['action' => 'MessagesController@store']) !!}
                <div class="compose_mail_main_area">
                    <div class="individual_reply_mail_heading_area">
                        <p><input class="input_style3 email" type="text" name="email" placeholder="Add Email ID" /></p>
                    </div>
                    <div class="compose_mail_subject_area">
                        <p><input class="input_style3" type="text" name="subject" placeholder="Subject" /></p>
                    </div>

                    <div class="individual_reply_mail_text_area">
                        <textarea class="individual_reply_mail_textarea_style2" name="content" placeholder="Message"></textarea>
                    </div>

                    <div class="individual_reply_mail_send_area">
                        <input type="submit" value="Send" class="send_but1" />
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <!--end_popup1_main_area-->
    </div>
@endif

<div id="login-box7" class="login-popup7">
    <a href="#" class="close2"><img src="{{ url('images/close_pop.png') }}" class="btn_close" title="Close Window" alt="Close" /></a>
    <!--start_popup1_main_area-->
    <div class="popup1_main_area4">

        <div class="individual_mail_main_area">
            <div class="individual_mail_heading_area">
                <div class="individual_mail_suject_and_text_area">
                    <h1 class="sender"></h1>
                    <p class="subject"></p>
                </div>
                <h2 class="time"></h2>
            </div>

            <div class="individual_mail_main_text">
                <p class="content"></p>
            </div>

            <div class="individual_mail_reply_area">
                <p><a href="javascript:show_div()">Reply</a></p>
            </div>
        </div>

        <script language="javascript">
            function show_div()
            {
                document.getElementById("show_box").style.display='';
                document.getElementById("hide_box").style.display='none';

            }
            function hide_div()
            {
                document.getElementById("show_box").style.display='none';
                document.getElementById("hide_box").style.display='';
            }
        </script>

        {!! Form::open(['action' => 'MessagesController@store']) !!}
            <div class="compose_mail_main_area" style="display:none" id="show_box">
                <div class="individual_reply_mail_heading_area">
                    <p><input class="input_style3 email" type="text" name="email" placeholder="Add Email ID" /></p>
                </div>
                <div class="compose_mail_subject_area">
                    <p><input class="input_style3" type="text" name="subject" placeholder="Subject" /></p>
                </div>

                <div class="individual_reply_mail_text_area">
                    <textarea class="individual_reply_mail_textarea_style2" name="content" placeholder="Message"></textarea>
                </div>

                <div class="individual_reply_mail_send_area">
                    <input type="submit" value="Send" class="send_but1" />
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    <!--end_popup1_main_area-->
</div>