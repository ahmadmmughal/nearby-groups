<div class="top_header_contanear">
    <div class="contanear">
        <div class="logo_area"><a href="{{ url('/') }}"><img src="{{ url('images/logo.png') }}" alt="" /></a></div>
        @if(Sentinel::guest())
            <div class="signup_login_but_main_area">
                <a href="#login-box" class="signup_but login-window"><img src="{{ url('images/sign_up_but.png') }}" alt="" /> Sign Up</a>
                <a href="#login-box2" class="login_but login-window2"><img src="{{ url('images/login_but.png') }}" alt="" /> Login</a>
            </div>
        @else
            <div class="login_main_area">
                <h1>Welcome - <a href="{{ route('user.show', ['id' => Sentinel::getUser()->id]) }}"><span>{{ Sentinel::getUser()->name }}</span></a></h1>
            </div>
        @endif
    </div>
</div>