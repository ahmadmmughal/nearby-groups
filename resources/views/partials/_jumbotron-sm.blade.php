<div class="header_contanear_subpage">
    <div class="contanear">
        <div class="header_subpage_main_area">
            <h1>Search Your <span>Nearby Groups</span></h1>
            {!! Form::open(['method' => 'GET', 'action' => 'SearchController@search']) !!}
                {!! Form::select('category', $categories, null, ['class' => 'subpage_header_search_select_style1']) !!}
                {!! Form::select('sub_category', $sub_categories, null, ['class' => 'subpage_header_search_select_style1']) !!}
                {!! Form::select('country', $countries, null, ['class' => 'subpage_header_search_select_style1']) !!}
                <input type="text" name="locality" class="subpage_header_search_input_style1" placeholder="Locality" />
                <input type="submit" value="Search" class="subpage_header_search_submit_style1" />
            {!! Form::close() !!}
        </div>
    </div>
</div>