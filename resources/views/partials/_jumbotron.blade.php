<div class="header_contanear">
    <div class="contanear">
        <div class="header_img_area">
            <img src="{{ url('images/header_img.png') }}" alt="" />
        </div>

        <div class="header_search_area">
            <h1>Search Your <span>Nearby Groups</span></h1>
            {!! Form::open(['method' => 'GET', 'action' => 'SearchController@search']) !!}
                <div class="header_full_w">
                    {!! Form::select('category', $categories, null, ['class' => 'header_search_select_style1']) !!}
                    {!! Form::select('sub_category', $sub_categories, null, ['class' => 'header_search_select_style1']) !!}
                </div>
                <div class="header_full_w">
                    {!! Form::select('country', $countries, null, ['class' => 'header_search_select_style1']) !!}
                    <input name="locality" type="text" class="header_search_input_style1" placeholder="Locality"  />
                </div>
                <input type="submit" value="Search" class="header_search_submit_style1" />
            {!! Form::close() !!}
        </div>
    </div>
</div>