<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public function groups(){
        return $this->hasMany('App\Group');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
