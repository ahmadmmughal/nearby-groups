<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function groups(){
        return $this->hasMany('App\Group');
    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function cities() {
        return $this->hasMany(City::class);
    }
}
