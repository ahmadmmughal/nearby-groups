<?php

namespace App\Providers;

use App\User;
use Sentinel;
use App\Category;
use App\City;
use App\Country;
use App\Locality;
use App\SubCategory;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['*'], function($view){
            $view->with('auth', Sentinel::getUser());
        });

        view()->composer(['*'], function($view){
            $view->with('countries', array('' => 'Select Country') + Country::orderBy('name')->lists('name', 'id')->toArray());
            $view->with('cities', array('' => 'Select Country First'));
            $view->with('categories',array('' => 'Select Category') + Category::orderBy('name')->lists('name', 'id')->toArray());
            $view->with('sub_categories', array('' => 'Select Category First'));
        });

//        view()->composer(['group.edit'], function($view){
//            $view->with('users', User::orderBy('name')->lists('name', 'id'));
//        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
