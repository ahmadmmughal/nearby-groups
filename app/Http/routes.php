<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Group;

Route::get('check', function(){
    $group = Group::find(2);

    dd($group->users->random(1));
});

Route::get('/', 'PagesController@home');
Route::get('search', 'SearchController@search');
Route::get('about', 'PagesController@about');
Route::get('terms', 'PagesController@terms');
Route::get('contact', 'PagesController@contact');
Route::get('privacy', 'PagesController@privacy');

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

Route::get('facebook_redirect', 'AuthController@facebookRedirect');
Route::get('facebook_login', 'AuthController@facebookLogin');
Route::get('verify_account', ['as' => 'auth.verify', 'uses' => 'AuthController@verify']);

Route::resource('user', 'UsersController');
Route::get('user/{id}/groups', ['as' => 'user.show_groups', 'uses' => 'UsersController@show_groups']);

Route::resource('group', 'GroupsController');
Route::post('groups/{group}/add_moderator', ['as' => 'group.add_moderator', 'uses' => 'GroupsController@addModerator']);
Route::delete('groups/{group}/remove_moderator', ['as' => 'group.remove_moderator', 'uses' => 'GroupsController@removeModerator']);
Route::get('groups/{group}/accept_member/{user}', ['as' => 'group.accept_member', 'uses' => 'GroupsController@acceptMember']);
Route::get('groups/{group}/reject_member/{user}', ['as' => 'group.reject_member', 'uses' => 'GroupsController@rejectMember']);
Route::get('group/{group}/join', ['as' => 'group.join', 'uses' => 'GroupsController@join']);
Route::get('group/{group}/leave', ['as' => 'group.leave', 'uses' => 'GroupsController@leave']);

Route::group(['prefix' => 'group/{group}'], function () {
    Route::resource('event', 'EventsController');
    Route::resource('post', 'PostsController');
});

Route::post('post/{post}/add_comment', ['as' => 'post.comment', 'uses' => 'CommentsController@store']);

Route::resource('message', 'MessagesController');

Route::post('request', 'AuthController@requestPasswordReset');
Route::get('reset_password', ['as' => 'password.reset', 'uses' => 'AuthController@performPasswordReset']);
Route::post('complete', 'AuthController@completePasswordReset');

Route::get('category/{category}/sub_categories', 'CategoriesController@getSubCategories');
Route::get('country/{country}/cities', 'CountriesController@getCities');