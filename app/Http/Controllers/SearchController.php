<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Group;
use App\SubCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    /**
     * Searches all the models against the keyword.
     *
     * @param Request $request
     * @return static
     */
    public function search(Request $request){
        $groups = collect();

        if($request->has('category')){
            $category = Category::find($request->input('category'));

            $groups->push($category->groups);
        }

        if($request->has('sub_category')){
            $sub_category = SubCategory::find($request->input('sub_category'));

            $groups->push($sub_category->groups);
        }

        if($request->has('country')){
            $country = City::find($request->input('country'));

            $groups->push($country->groups);
        }

        if($request->has('city')){
            $city = City::find($request->input('city'));

            $groups->push($city->groups);
        }

        if($request->has('locality')){
            $locality = $request->input('locality');

            $result = Group::where('locality', 'like', '%' . $locality . '%')->get();

            $groups->push($result);
        }

//        dd($groups);

        $search_results = $groups->collapse()->unique('id');

        $data['active_groups'] = $search_results;
        $data['heading'] = 'Search Results';

        return view('pages.home', $data);
    }
}
