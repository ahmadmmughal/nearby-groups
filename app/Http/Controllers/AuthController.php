<?php

namespace App\Http\Controllers;

use Reminder;
use Activation;
use Socialite;
use App\User;
use Sentinel;
use Mail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(Request $request){
        $this->validate($request, ['name' => 'required', 'email' => 'required|email|unique:users,email', 'password' => 'required|min:6', 'confirm_password' => 'required|same:password']);

//        dd($request);

        $user = Sentinel::register($request->all());

        $activation = Activation::create($user);

        Mail::send('emails.verification', ['id' => $user->id, 'code' => $activation->code], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Welcome to Nearby Groups!');
        });

        $request->session()->flash('error', 'An activation email has been sent to the email address you provided.');

        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request){
        $this->validate($request, ['email' => 'required|email|exists:users,email', 'password' => 'required']);

        $credentials = [
            'login' => $request->input('email')
        ];

        $user = Sentinel::findByCredentials($credentials);

        if(!$activation = Activation::completed($user)) {
            $request->session()->flash('error', 'Please activate your account.');
            return redirect('/');
        }

        ($request->has('remember')) ? Sentinel::authenticateAndRemember($request->all()) : Sentinel::authenticate($request->all());

        return redirect('/');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(){
        Sentinel::logout();

        return redirect('/');
    }

    /**
     *
     */
    public function facebookRedirect(){
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request){
        $user = Sentinel::findById($request->id);

        $activation = Activation::exists($user);

        if($activation == false){
            return redirect()->to('/');
        }

        if(Activation::complete($user, $request->code)){
            Sentinel::login($user);
        }

        return redirect()->to('/');
    }

    /**
     *
     */
    public function facebookLogin(){
        $socialite_user = Socialite::driver('facebook')->user();

//        dd($socialite_user);

        /*
         * Check if the requested Facebook User already exists.
         */
        $user = User::where('facebook_id', $socialite_user->id)->first();

        if ( empty($user) ){

            /*
             * If it doesnt exist then check if the requested User has normally signed for Makerheart.
             */
            $user = Sentinel::findByCredentials(['login' => $socialite_user->email]);

            if( empty($user) ){

                /*
                 * If the User hasnt signed up before. Create a new account for the user.
                 */
                $user = Sentinel::registerAndActivate([
                    'email' => $socialite_user->email,
                    'password' => str_random(8),
                    'image' => $socialite_user->avatar,
                    'name' => $socialite_user->name,
                    'facebook_id' => $socialite_user->id
                ]);
            }
        }

        /*
         * Log the Facebook User in.
         */
        Sentinel::login($user);

        return redirect('/');
    }

    public function requestPasswordReset(Request $request){
        $user = User::where('email', $request->email)->first();

        if( $user ){
            ($reminder = Reminder::exists($user)) || ($reminder = Reminder::create($user));
        }
        else{
            return redirect()->to('/');
        }

        $data = [
            'email' => $user->email,
            'name' => $user->name,
            'subject' => 'Reset Your Password',
            'code' => $reminder->code,
            'id' => $user->id
        ];

        Mail::queue('emails.password_reset', $data, function($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject($data['subject']);
        });

        return redirect()->to('/');
    }

    public function performPasswordReset(Request $request) {
        $user = Sentinel::findById($request->id);

        if (Reminder::exists($user, $request->code)) {
            return view()->make('pages.password_reset', ['id' => $request->id, 'code' => $request->code]);
        }
        else {
            //incorrect info was passed
            return redirect()->to('/');
        }
    }

    public function completePasswordReset(Request $request){
        $password = $request->input('password');
        $passwordConf = $request->input('password_confirmation');

        $user = Sentinel::findById($request->query('id'));
        $reminder = Reminder::exists($user, $request->query('code'));

        //incorrect info was passed.
        if ($reminder == false) {
            return redirect()->to('/');
        }

        if ($password != $passwordConf) {
//            flash()->error('Passwords must match.');
            return view()->make('pages.password_reset', ['id' => $request->query('id'), 'code' => $request->query('code')]);
        }


        if(Reminder::complete($user, $request->query('code'), $password)){
//            flash()->success('Your password has been successfully reset.');
        }
        else{
//            flash()->error('There was a problem in resetting your password. Please try again.');
        }

        return redirect()->to('/');
    }
}
