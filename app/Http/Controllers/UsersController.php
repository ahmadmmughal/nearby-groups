<?php

namespace App\Http\Controllers;

use Sentinel;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data['user'] = User::findOrFail($id);

        $auth = Sentinel::getUser();

        if(Sentinel::guest() || ($auth->id != $data['user']->id) ){
            return view('profile.show_guest', $data);
        }
        else{
            return view('profile.show', $data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);

        return view('profile.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $this->validate($request, ['name' => 'required', 'email' => 'required|email|unique:users,email,' . $user->id]);

        $user->fill($request->except('image'));

        $user->image = ($request->hasFile('image')) ? $this->upload($request->file('image')) : $user->image;

        $user->save();

        return redirect()->route('user.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if( !$user->groups_joined_as_admin->isEmpty() ){
            foreach($user->groups_joined_as_admin as $group){
                $group->appointNewAdmin();
            }
        }

        $user->delete();

        return redirect()->to('/');
    }

    public function show_groups($id){
        $data['user'] = User::findOrFail($id);

        return view('profile.groups', $data);
    }
}
