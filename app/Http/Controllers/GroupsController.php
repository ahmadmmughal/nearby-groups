<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['user'] = Sentinel::getUser();

        return view('group.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'privacy' => 'required']);

        $user = Sentinel::getUser();

        $group = new Group($request->except('image'));

        $group->image = ($request->hasFile('image')) ? $this->upload($request->file('image')) : null;

        $group->save();

        $group->users()->attach($user, ['group_role_id' => GROUP_ADMIN, 'status' => APPROVED_MEMBER]);

        return redirect()->route('group.show', ['id' => $group->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data['group'] = Group::findOrFail($id);

        if(Sentinel::guest()){
            return view('group.show_guest', $data);
        }
        else{
            return view('group.show', $data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data['group'] = Group::findOrFail($id);

        return view('group.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'privacy' => 'required']);

        $group = Group::find($id);

        $group->fill($request->except('image'));

        $group->image = ($request->hasFile('image')) ? $this->upload($request->file('image')) : $group->image;

        $group->save();

        return redirect()->route('group.show', ['id' => $group->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);

        $group->delete();

        return redirect()->to('/');
    }

    public function join($id){
        $user = Sentinel::getUser();

        $group = Group::find($id);

        $status = ( $group->privacy == PRIVATE_GROUP ) ? PENDING_MEMBER : APPROVED_MEMBER;

        $group->users()->attach($user, ['status' => $status]);

        return redirect()->route('group.show', ['group' => $group->id]);
    }

    public function leave($id){
        $user = Sentinel::getUser();

        $group = Group::find($id);

        if( $group->admin->id == $user->id ){
            $group->appointNewAdmin();
        }

        $group->users()->detach($user);

        return redirect()->route('group.show', ['group' => $group->id]);
    }

    public function addModerator(Request $request, $group){
        $moderator = User::find($request->input('user'));

        $group = Group::find($group);

        $group->users()->detach($moderator);
        $group->users()->attach($moderator, ['group_role_id' => GROUP_MODERATOR]);

        return redirect()->back();
    }

    public function removeModerator(Request $request, $group){
        $moderator = User::find($request->input('user'));

        $group = Group::find($group);

        $group->users()->detach($moderator);
        $group->users()->attach($moderator, ['group_role_id' => 0]);

        return redirect()->back();
    }

    public function acceptMember($group, $user){
        $user = User::find($user);

        $group = Group::find($group);

        $group->all_users()->detach($user);
        $group->all_users()->attach($user, ['group_role_id' => 0, 'status' => APPROVED_MEMBER]);

        return redirect()->back();
    }

    public function rejectMember($group, $user){
        $user = User::find($user);

        $group = Group::find($group);

        $group->all_users()->detach($user);

        return redirect()->back();
    }
}
