<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function home(){
        $groups = Group::all();

        $active_groups = $groups->sortByDesc('member_count')->take(10);

        $data['active_groups'] = $active_groups;
        $data['heading'] = 'Most Active Groups';

        return view('pages.home', $data);
    }

    public function terms(){
        return view('pages.terms');
    }

    public function privacy(){
        return view('pages.privacy');
    }

    public function about(){
        return view('pages.about');
    }

    public function contact(){
        return view('pages.contact');
    }
}
