<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    public function getCities($country) {
        $country = Country::find($country);

        return $country->cities;
    }
}
