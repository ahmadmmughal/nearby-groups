<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function getSubCategories($category) {
        $category = Category::find($category);

        return $category->sub_categories;
    }
}
