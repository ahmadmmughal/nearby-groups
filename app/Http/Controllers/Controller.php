<?php

namespace App\Http\Controllers;

use Image;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function upload(UploadedFile $file){
        if($file->isValid()){

            $filename = time(). str_random(4) . "." . $file->getClientOriginalExtension();

            Image::make($file)->save(public_path('uploads/' . $filename));

            /*
             * Generate thumbnail.
             */
            Image::make(public_path('uploads/' . $filename))->fit(300)->save(public_path('uploads/tn-' . $filename));

            return $filename;
        }
    }
}
