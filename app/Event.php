<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'venue', 'date', 'map_id'];

    protected $dates = ['date'];

    public function setDateAttribute($date){
        $this->attributes['date'] = Carbon::parse($date);
    }
}
