<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image', 'description', 'locality', 'category_id', 'city_id', 'country_id', 'sub_category_id', 'privacy'];

    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    |
    */

    public function getThumbnailUrlAttribute(){
        if( !empty($this->image) )
            return url('uploads/tn-' . $this->image);

        return url('images/group_placeholder.jpg');
    }

    public function getAdminAttribute(){
        return $this->admins()->first();
    }

    public function getMemberCountAttribute(){
        return count($this->members);
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function all_users(){
        return $this->belongsToMany('App\User')->withPivot('user_id', 'group_id', 'group_role_id');
    }

    public function users(){
        return $this->belongsToMany('App\User')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('status', APPROVED_MEMBER);
    }

    public function pending_users(){
        return $this->belongsToMany('App\User')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('status', PENDING_MEMBER);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function members(){
        return $this->belongsToMany('App\User')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('group_role_id', 0)->wherePivot('status', APPROVED_MEMBER);
    }

    public function admins(){
        return $this->belongsToMany('App\User')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('group_role_id', GROUP_ADMIN)->wherePivot('status', APPROVED_MEMBER);
    }

    public function moderators(){
        return $this->belongsToMany('App\User')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('group_role_id', GROUP_MODERATOR)->wherePivot('status', APPROVED_MEMBER);
    }

    public function events(){
        return $this->hasMany('App\Event');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function appointNewAdmin(){
        if( !$this->moderators->isEmpty() ){
            $moderator = $this->moderators->random(1);

            $this->users()->detach($moderator);
            $this->users()->attach($moderator, ['group_role_id' => GROUP_ADMIN]);
        }
        else if( !$this->members->isEmpty() ){
            $member = $this->members->random(1);

            $this->users()->detach($member);
            $this->users()->attach($member, ['group_role_id' => GROUP_ADMIN]);
        }
        else{
            $this->delete();
        }
    }
}
