<?php

namespace App;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends EloquentUser{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'bio', 'hobbies', 'city_id', 'country_id', 'facebook_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    |
    */

    public function getImageUrlAttribute(){
        return url('uploads/' . $this->image);
    }

    public function getThumbnailUrlAttribute(){

        if(!empty($this->image))
            return url('uploads/tn-' . $this->image);

        return url('images/demo_profile.jpg');
    }

    public function getGroupsJoinedAsUserAttribute(){
        return $this->groups()->where('group_role_id', 0)->get();
    }

    public function getGroupsJoinedAsAdminAttribute(){
        return $this->groups()->where('group_role_id', GROUP_ADMIN)->get();
    }

    public function getGroupsJoinedAsModeratorAttribute(){
        return $this->groups()->where('group_role_id', GROUP_MODERATOR)->get();
    }

    public function getFeedPostsAttribute(){
        $feed_posts = collect();

        foreach($this->groups as $group){
            $feed_posts->push($group->posts);
        }

        $result = $feed_posts->collapse()->sortByDesc('created_at');

        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function groups(){
        return $this->belongsToMany('App\Group')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('status', APPROVED_MEMBER);
    }

    public function pending_groups(){
        return $this->belongsToMany('App\Group')->withPivot('user_id', 'group_id', 'group_role_id')->wherePivot('status', PENDING_MEMBER);
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function posts(){
        return $this->hasMany('App\Posts');
    }

    public function received_messages(){
        return $this->hasMany('App\Message', 'recipient_id');
    }

    public function sent_messages(){
        return $this->hasMany('App\Message', 'sender_id');
    }
}
