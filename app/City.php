<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function groups(){
        return $this->hasMany('App\Group');
    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function country() {
        return $this->belongsTo(Country::class);
    }
}
