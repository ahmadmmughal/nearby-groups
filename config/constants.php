<?php

/*
|--------------------------------------------------------------------------
| Global Role Constants
|--------------------------------------------------------------------------
|
*/

//define('MAKERHEART_ADMIN', 1);
//define('MAKERHEART_EDITOR', 2);
//define('DESIGNER', 3);
//define('USER', 4);

/*
|--------------------------------------------------------------------------
| Company Role Constants
|--------------------------------------------------------------------------
|
*/

define('GROUP_ADMIN', 1);
define('GROUP_MODERATOR', 2);

define('PENDING_MEMBER', 1);
define('APPROVED_MEMBER', 2);

define('PRIVATE_GROUP', 0);
define('PUBLIC_GROUP', 1);