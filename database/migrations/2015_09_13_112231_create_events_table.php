<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->integer('group_id');
            $table->date('date');
            $table->string('venue');
            $table->integer('map_id')->nullable();
//            $table
//                ->foreign('map_id')
//                ->references('id')->on('maps')
//                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('events', function(Blueprint $table) {
//            $table
//                ->dropForeign('events_map_id_foreign');
//        });

        Schema::drop('events');
    }
}
