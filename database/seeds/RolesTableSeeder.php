<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentinel::getRoleRepository()->createModel()->create([
            'id' => 1,
            'name' => 'Site Admin',
            'slug' => 'site-admin',
        ]);
    }
}
