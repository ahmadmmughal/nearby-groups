<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('countries')->insert([
            [
                'id' => 1,
                'name' => 'India'
            ]
        ]);

        DB::table('cities')->insert([
            [
                'id' => 1, 'country_id' => 1, 'name' => 'Andhra Pradesh - Guntur'
            ],
            [
                'id' => 2, 'country_id' => 1, 'name' => 'Andhra Pradesh - Nellore'
            ],
            [
                'id' => 3, 'country_id' => 1, 'name' => 'Andhra Pradesh - Vijayawada'
            ],
            [
                'id' => 4, 'country_id' => 1, 'name' => 'Andhra Pradesh - Visakhapatnam'
            ],
            [
                'id' => 5, 'country_id' => 1, 'name' => 'Assam - Guwahati'
            ],
            [
                'id' => 6, 'country_id' => 1, 'name' => 'Bihar - Gaya'
            ],
            [
                'id' => 7, 'country_id' => 1, 'name' => 'Bihar - Patna'
            ],
            [
                'id' => 8, 'country_id' => 1, 'name' => 'Chandigarh - Chandigarh'
            ],
            [
                'id' => 9, 'country_id' => 1, 'name' => 'Chhattisgarh - Bhilai'
            ],
            [
                'id' => 10, 'country_id' => 1, 'name' => 'Chhattisgarh - Raipur'
            ],
            [
                'id' => 11, 'country_id' => 1, 'name' => 'Delhi - Delhi'
            ],
            [
                'id' => 12, 'country_id' => 1, 'name' => 'Gujarat - Ahmedabad'
            ],
            [
                'id' => 13, 'country_id' => 1, 'name' => 'Gujarat - Bhavnagar'
            ],
            [
                'id' => 14, 'country_id' => 1, 'name' => 'Gujarat - Jamnagar'
            ],
            [
                'id' => 15, 'country_id' => 1, 'name' => 'Gujarat - Rajkot'
            ],
            [
                'id' => 16, 'country_id' => 1, 'name' => 'Gujarat - Surat'
            ],
            [
                'id' => 17, 'country_id' => 1, 'name' => 'Gujarat - Vadodara'
            ],
            [
                'id' => 18, 'country_id' => 1, 'name' => 'Haryana - Faridabad'
            ],
            [
                'id' => 19, 'country_id' => 1, 'name' => 'Haryana - Gurgaon'
            ],
            [
                'id' => 20, 'country_id' => 1, 'name' => 'Jammu and Kashmir - Anantnag'
            ],
            [
                'id' => 21, 'country_id' => 1, 'name' => 'Jammu and Kashmir - Jammu'
            ],
            [
                'id' => 22, 'country_id' => 1, 'name' => 'Jammu and Kashmir - Srinagar'
            ],
            [
                'id' => 23, 'country_id' => 1, 'name' => 'Jharkhand - Dhanbad'
            ],
            [
                'id' => 24, 'country_id' => 1, 'name' => 'Jharkhand - Jamshedpur'
            ],
            [
                'id' => 25, 'country_id' => 1, 'name' => 'Jharkhand - Ranchi'
            ],
            [
                'id' => 26, 'country_id' => 1, 'name' => 'Karnataka - Bangalore'
            ],
            [
                'id' => 27, 'country_id' => 1, 'name' => 'Karnataka - Belgaum'
            ],
            [
                'id' => 28, 'country_id' => 1, 'name' => 'Karnataka - Gulbarga'
            ],
            [
                'id' => 29, 'country_id' => 1, 'name' => 'Karnataka - Hubballi-Dharwad'
            ],
            [
                'id' => 30, 'country_id' => 1, 'name' => 'Karnataka - Mangalore'
            ],
            [
                'id' => 31, 'country_id' => 1, 'name' => 'Karnataka - Mysore'
            ],
            [
                'id' => 32, 'country_id' => 1, 'name' => 'Kerala - Kochi'
            ],
            [
                'id' => 33, 'country_id' => 1, 'name' => 'Kerala - Thiruvananthapuram'
            ],
            [
                'id' => 34, 'country_id' => 1, 'name' => 'Madhya Pradesh - Bhopal'
            ],
            [
                'id' => 35, 'country_id' => 1, 'name' => 'Madhya Pradesh - Gwalior'
            ],
            [
                'id' => 36, 'country_id' => 1, 'name' => 'Madhya Pradesh - Indore'
            ],
            [
                'id' => 37, 'country_id' => 1, 'name' => 'Madhya Pradesh - Jabalpur'
            ],
            [
                'id' => 38, 'country_id' => 1, 'name' => 'Madhya Pradesh - Ujjain'
            ],
            [
                'id' => 39, 'country_id' => 1, 'name' => 'Maharashtra - Amravati'
            ],
            [
                'id' => 40, 'country_id' => 1, 'name' => 'Maharashtra - Aurangabad'
            ],
            [
                'id' => 41, 'country_id' => 1, 'name' => 'Maharashtra - Bhiwandi'
            ],
            [
                'id' => 42, 'country_id' => 1, 'name' => 'Maharashtra - Jalgaon'
            ],
            [
                'id' => 43, 'country_id' => 1, 'name' => 'Maharashtra - Kalyan-Dombivali'
            ],
            [
                'id' => 44, 'country_id' => 1, 'name' => 'Maharashtra - Kolhapur'
            ],
            [
                'id' => 45, 'country_id' => 1, 'name' => 'Maharashtra - Malegaon'
            ],
            [
                'id' => 46, 'country_id' => 1, 'name' => 'Maharashtra - Mira-Bhayandar'
            ],
            [
                'id' => 47, 'country_id' => 1, 'name' => 'Maharashtra - Mumbai'
            ],
            [
                'id' => 48, 'country_id' => 1, 'name' => 'Maharashtra - Nagpur'
            ],
            [
                'id' => 49, 'country_id' => 1, 'name' => 'Maharashtra - Nanded'
            ],
            [
                'id' => 50, 'country_id' => 1, 'name' => 'Maharashtra - Nashik'
            ],
            [
                'id' => 51, 'country_id' => 1, 'name' => 'Maharashtra - Navi Mumbai'
            ],
            [
                'id' => 52, 'country_id' => 1, 'name' => 'Maharashtra - Pimpri-Chinchwad'
            ],
            [
                'id' => 53, 'country_id' => 1, 'name' => 'Maharashtra - Pune'
            ],
            [
                'id' => 54, 'country_id' => 1, 'name' => 'Maharashtra - Sangli-Miraj & Kupwad'
            ],
            [
                'id' => 55, 'country_id' => 1, 'name' => 'Maharashtra - Solapur'
            ],
            [
                'id' => 56, 'country_id' => 1, 'name' => 'Maharashtra - Thane'
            ],
            [
                'id' => 57, 'country_id' => 1, 'name' => 'Maharashtra - Ulhasnagar'
            ],
            [
                'id' => 58, 'country_id' => 1, 'name' => 'Maharashtra - Vasai-Virar'
            ],
            [
                'id' => 59, 'country_id' => 1, 'name' => 'Orissa - Bhubaneswar'
            ],
            [
                'id' => 60, 'country_id' => 1, 'name' => 'Orissa - Cuttack'
            ],
            [
                'id' => 61, 'country_id' => 1, 'name' => 'Punjab - Amritsar'
            ],
            [
                'id' => 62, 'country_id' => 1, 'name' => 'Punjab - Jalandhar'
            ],
            [
                'id' => 63, 'country_id' => 1, 'name' => 'Punjab - Ludhiana' ],[ 'id' => 64, 'country_id' => 1, 'name' => 'Rajasthan - Ajmer' ],[ 'id' => 65, 'country_id' => 1, 'name' => 'Rajasthan - Bikaner' ],[ 'id' => 66, 'country_id' => 1, 'name' => 'Rajasthan - Jaipur' ],[ 'id' => 67, 'country_id' => 1, 'name' => 'Rajasthan - Jodhpur' ],[ 'id' => 68, 'country_id' => 1, 'name' => 'Rajasthan - Kota' ],[ 'id' => 69, 'country_id' => 1, 'name' => 'Rajasthan - Udaipur' ],[ 'id' => 70, 'country_id' => 1, 'name' => 'Tamil Nadu - Ambattur' ],[ 'id' => 71, 'country_id' => 1, 'name' => 'Tamil Nadu - Chennai' ],[ 'id' => 72, 'country_id' => 1, 'name' => 'Tamil Nadu - Coimbatore' ],[ 'id' => 73, 'country_id' => 1, 'name' => 'Tamil Nadu - Madurai' ],[ 'id' => 74, 'country_id' => 1, 'name' => 'Tamil Nadu - Salem' ],[ 'id' => 75, 'country_id' => 1, 'name' => 'Tamil Nadu - Tiruchirappalli' ],[ 'id' => 76, 'country_id' => 1, 'name' => 'Tamil Nadu - Tirunelveli' ],[ 'id' => 77, 'country_id' => 1, 'name' => 'Telangana - Hyderabad' ],[ 'id' => 78, 'country_id' => 1, 'name' => 'Telangana - Warangal' ],[ 'id' => 79, 'country_id' => 1, 'name' => 'Uttar Pradesh - Agra' ],[ 'id' => 80, 'country_id' => 1, 'name' => 'Uttar Pradesh - Aligarh' ],[ 'id' => 81, 'country_id' => 1, 'name' => 'Uttar Pradesh - Allahabad' ],[ 'id' => 82, 'country_id' => 1, 'name' => 'Uttar Pradesh - Bareilly' ],[ 'id' => 83, 'country_id' => 1, 'name' => 'Uttar Pradesh - Firozabad' ],[ 'id' => 84, 'country_id' => 1, 'name' => 'Uttar Pradesh - Ghaziabad' ],[ 'id' => 85, 'country_id' => 1, 'name' => 'Uttar Pradesh - Gorakhpur' ],[ 'id' => 86, 'country_id' => 1, 'name' => 'Uttar Pradesh - Jhansi' ],[ 'id' => 87, 'country_id' => 1, 'name' => 'Uttar Pradesh - Kanpur' ],[ 'id' => 88, 'country_id' => 1, 'name' => 'Uttar Pradesh - Loni' ],[ 'id' => 89, 'country_id' => 1, 'name' => 'Uttar Pradesh - Lucknow' ],[ 'id' => 90, 'country_id' => 1, 'name' => 'Uttar Pradesh - Meerut' ],[ 'id' => 91, 'country_id' => 1, 'name' => 'Uttar Pradesh - Moradabad' ],[ 'id' => 92, 'country_id' => 1, 'name' => 'Uttar Pradesh - Noida' ],[ 'id' => 93, 'country_id' => 1, 'name' => 'Uttar Pradesh - Saharanpur' ],[ 'id' => 94, 'country_id' => 1, 'name' => 'Uttar Pradesh - Varanasi' ],[ 'id' => 95, 'country_id' => 1, 'name' => 'Uttarakhand - Dehradun' ],[ 'id' => 96, 'country_id' => 1, 'name' => 'West Bengal - Asansol' ],[ 'id' => 97, 'country_id' => 1, 'name' => 'West Bengal - Durgapur' ],[ 'id' => 98, 'country_id' => 1, 'name' => 'West Bengal - Howrah' ],[ 'id' => 99, 'country_id' => 1, 'name' => 'West Bengal - Kolkata' ],[ 'id' => 100, 'country_id' => 1, 'name' => 'West Bengal - Siliguri' ]]);


        DB::table('categories')->insert([
            [
                'id' => 1,
                'name' => 'Sports'
            ]
        ]);

        DB::table('sub_categories')->insert([
            [
                'id' => 1,
                'category_id' => 1,
                'name' => 'Aerobatics / Gymnastics
'
            ],
            [
                'id' => 2,
                'category_id' => 1,
                'name' => 'Badminton'
            ],
            [
                'id' => 3,
                'category_id' => 1,
                'name' => 'Basketball'
            ],
            [
                'id' => 4,
                'category_id' => 1,
                'name' => 'Billiards'
            ],
            [
                'id' => 5,
                'category_id' => 1,
                'name' => 'Boxing'
            ],
            [
                'id' => 6,
                'category_id' => 1,
                'name' => 'Carom'
            ],
            [
                'id' => 7,
                'category_id' => 1,
                'name' => 'Chess'
            ],
            [
                'id' => 8,
                'category_id' => 1,
                'name' => 'Cricket'
            ],
            [
                'id' => 9,
                'category_id' => 1,
                'name' => 'Cross Country'
            ],
            [
                'id' => 10,
                'category_id' => 1,
                'name' => 'Cycling'
            ],
            [
                'id' => 11,
                'category_id' => 1,
                'name' => 'Fishing'
            ],
            [
                'id' => 12,
                'category_id' => 1,
                'name' => 'Football'
            ],
            [
                'id' => 13,
                'category_id' => 1,
                'name' => 'Golf'
            ],
            [
                'id' => 14,
                'category_id' => 1,
                'name' => 'Hiking'
            ],
            [
                'id' => 15,
                'category_id' => 1,
                'name' => 'Hockey'
            ],
            [
                'id' => 16,
                'category_id' => 1,
                'name' => 'Martial Arts'
            ],
            [
                'id' => 17,
                'category_id' => 1,
                'name' => 'Paragliding'
            ],
            [
                'id' => 18,
                'category_id' => 1,
                'name' => 'Polo'
            ],
            [
                'id' => 19,
                'category_id' => 1,
                'name' => 'Rafting'
            ],
            [
                'id' => 20,
                'category_id' => 1,
                'name' => 'Running'
            ],
            [
                'id' => 21,
                'category_id' => 1,
                'name' => 'Skating'
            ],
            [
                'id' => 22,
                'category_id' => 1,
                'name' => 'Snooker'
            ],
            [
                'id' => 23,
                'category_id' => 1,
                'name' => 'Squash'
            ],
            [
                'id' => 24,
                'category_id' => 1,
                'name' => 'Swimming'
            ],
            [
                'id' => 25,
                'category_id' => 1,
                'name' => 'Tennis'
            ],
            [
                'id' => 26,
                'category_id' => 1,
                'name' => 'Volleyball'
            ]
        ]);

        Model::reguard();
    }
}
