<?php

use Illuminate\Database\Seeder;

class GroupRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group_roles')->insert([
            [
                'id' => 1,
                'name' => 'Admin',
                'slug' => 'admin',
            ],
            [
                'id' => 2,
                'name' => 'Moderator',
                'slug' => 'mod',
            ],
            [
                'id' => 3,
                'name' => 'User',
                'slug' => 'user',
            ]
        ]);
    }
}
